#!/bin/bash

cd "$(dirname "$0")" || exit

set -euo pipefail

# Run Celery on bg
/usr/istra/apps/pypy3.6-v7.3.1-linux64/bin/celery worker -A web_intervention_server.celery --loglevel=info --concurrency=10 & pid1=$!
trap on_finish EXIT
on_finish() { kill -SIGINT $pid1 $pid2 ; wait; }  # not strictly required as we didn't add 'set -m' flag

# Run Flower (Celery monitor), listens to port 5555
(/usr/istra/apps/pypy3.6-v7.3.1-linux64/bin/celery -A web_intervention_server.celery flower || echo "Failed running Flower Celery monitor (not critical)") & pid2=$!

# For development - run FastAPI debug server
./web_intervention_server.py


