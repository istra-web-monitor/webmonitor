from werkzeug.security import generate_password_hash, check_password_hash
from configs.monitor import TSO_USERNAMES
from configs.owners import IT_OWNERS

_users = dict(
    istra=generate_password_hash("istra1234"),
    arik=generate_password_hash("arik1234"),
    yaniv=generate_password_hash("yaniv1234"),
    laurence=generate_password_hash("laurence1234"),
    osama=generate_password_hash("osama1234"),
    tzach=generate_password_hash("tzach1234"),
)
_users['ilan-istra'] = generate_password_hash("ilan1234")

for username in TSO_USERNAMES:
    _users[username]=generate_password_hash("tso1234")

for username in IT_OWNERS:
    _users[username]=generate_password_hash("it1234")

def verify_password(username, password):
    if username in _users:
        return check_password_hash(_users[username], password)
    
    return False

READ_ONLY_USERS = ['istra']
