import json, os
from configs import regions
from web_intervention.reals import current_date

def get_ports(region):
    r = regions.REGIONS[region]
    date = current_date.get_date(tz=r.tzinfo)

    for sched_name in r.traded_sched_names_of_date(date=date):
        filename = os.path.join("/mnt/shared/sched", sched_name.name, date.strftime("%Y%m%d"), "ports.json")
        with open(filename, "rt") as f:
            data = json.load(f)
            for row in data:
                yield row
