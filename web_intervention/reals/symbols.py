import os, csv 
from configs import regions
from web_intervention.reals import current_date

def get_symbols(region):
    r = regions.REGIONS[region]
    date = current_date.get_date(tz=r.tzinfo)

    for sched_name in r.traded_sched_names_of_date(date=date):
        filename = os.path.join("/mnt/shared/sched", sched_name.name, date.strftime("%Y%m%d"), "traded_symbols.csv")
        with open(filename, "rt") as f:
            for row in csv.DictReader(f):
                row["strategyTag"] = row["trader"]
                del row["trader"]
                yield row

def get_tre_symbols_and_groups(region):
    r = regions.REGIONS[region]
    date = current_date.get_date(tz=r.tzinfo)

    for sched_name in r.traded_sched_names_of_date(date=date):
        filename = os.path.join("/mnt/shared/sched", sched_name.name, date.strftime("%Y%m%d"), "tre.limits.symbols.summary.csv")
        with open(filename, "rt") as f:
            for row in csv.DictReader(f):
                yield {k.replace(".", "_"): row[k] for k in ["symbol", "trader.family", "strategy", "group.name", "node.id"]}
