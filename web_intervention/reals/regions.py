#!/usr/istra/bin/pypy3
import os, csv, datetime, socket, pytz
from configs import regions, prod_servers
from configs.paths import current_repo_path
import math
from typing import List, cast
from typing_extensions import TypedDict
from configs.monitor import MONITOR_SERVERS
from web_intervention.reals import current_date

class DictReaderStrip(csv.DictReader):
    @property
    def fieldnames(self):
        if self._fieldnames is None:
            csv.DictReader.fieldnames.fget(self)
            if self._fieldnames is not None:
                self._fieldnames = [name.strip() for name in self._fieldnames]
        return self._fieldnames
    
    def __next__(self):
        d = super().__next__()
        return {k: v.strip() for k, v in d.items()}

def get_lmm_symbols() -> List[str]:
    path = os.path.join(current_repo_path, "R", "istrainfra", "data", "lmm.symbols.csv")
    symbols = set()

    def parse_date(d: str) -> float:
        if d == "-Inf":
            return -math.inf
        if d == "Inf":
            return math.inf
        return int(d)

    with open(path, "rt") as f:
        reader = DictReaderStrip(f)
        for row in reader:
            today = int(datetime.datetime.today().strftime("%Y%m%d"))
            if parse_date(row["from"]) <= today < parse_date(row["to"]):
                symbols.add(row["symbol"])
    return list(symbols)

class RegionServerNodeTypedDict(TypedDict):
    id: int
    role: str
    pretty_name: str
    is_special: bool

class RegionServerTypedDict(TypedDict):
    hostname: str
    sched_name: str
    server_type: str
    nodes: List[RegionServerNodeTypedDict]

class RegionTypedDict(TypedDict):
    timezone: str
    name: str
    sched_names: List[str]
    rle_servers: List[str]
    rle_accounts: List[str]
    lmm_symbols: List[str]
    servers: List[RegionServerTypedDict]
    
def get_regions() -> List[RegionTypedDict]:
    date = current_date.get_date()
    traded_sched_names = {s.name for r in regions.REGIONS.values() for s in r.sched_names if s.is_trading_date(date=date)}
    sched_names = lambda r: [s.name for s in r.sched_names if s.name in traded_sched_names]
    rle_accounts = lambda r: ["@GLOBAL"] + (["Pundion", "IssarCA", "USFutures"] if r.name == "America" else sched_names(r))
    lmm_symbols = lambda r: get_lmm_symbols() if r.name == "America" else list()
    data = [
        RegionTypedDict(
            timezone=str(r.tzinfo),
            name=r.name,
            sched_names=sched_names(r),
            rle_servers=[rs.hostname for rs in r.rle_servers],
            rle_accounts=rle_accounts(r),
            lmm_symbols=lmm_symbols(r),
            servers=[
                RegionServerTypedDict(
                    hostname=s.hostname,
                    sched_name=s.sched_name.name,
                    server_type=str(s.server_type),
                    nodes=[cast(RegionServerNodeTypedDict, dict(id=n.node_id, role=str(n.role), pretty_name=n.pretty_name, is_special=n.is_special, **{k: v for k, v in n.__dict__.items() if k.startswith("is_")})) for n in s.nodes.values() if n.is_active])
                for s in r.servers if str(s.server_type) in ('PRODUCTION', 'INTEGRATION', 'RECORDING') and s.sched_name.name in traded_sched_names
            ],
        ) for r in regions.REGIONS.values()]
    
    return data

def get_production_servers(r: str) -> List[int]:
    return [s.server_id for s in regions.REGIONS[r].production_servers]

def get_production_trading_nodes(r: str) -> List[int]:
    return [n.node_id for s in regions.REGIONS[r].production_servers for n in s.nodes.values() if n.role == prod_servers.node_roles.TRADING]

def get_timezone(r: str) -> pytz.BaseTzInfo:
    return regions.REGIONS[r].tzinfo

_my_region = None
def get_current_region() -> str:
    def _find_region():
        if "REGION" in os.environ:
            return os.environ["REGION"]
        hostname = socket.gethostname()
        if hostname in MONITOR_SERVERS:
            return MONITOR_SERVERS[hostname].region.name
        return "Europe"
    global _my_region
    if _my_region is None:
        _my_region = _find_region()
    return _my_region
