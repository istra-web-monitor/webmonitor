from collections import defaultdict
from configs import exchanges
d = defaultdict(list)
for ex in exchanges.EXCHANGES.values():
    d[ex.asset.region.name].append(dict(name=ex.name, istra_mark=ex.istra_mark, protocols=ex.protocols))

EXCHANGES = dict(d)

