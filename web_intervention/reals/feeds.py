from collections import defaultdict
from configs import feeds
d = defaultdict(list)
for feed in feeds.FEEDS.values():
    d[feed.assets[0].region.name].append(dict(name=feed.name, istra_mark=feed.istra_mark))

FEEDS = dict(d)


