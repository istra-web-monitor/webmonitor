import datetime, os, pytz
from typing import Optional

def get_date(tz: Optional[pytz.BaseTzInfo] = None) -> datetime.date:
    if "DATE_OVERRIDE" in os.environ:
        date_str = os.environ["DATE_OVERRIDE"]
        return datetime.datetime.strptime(date_str, "%Y%m%d").date()
    return datetime.datetime.now(tz).today()
