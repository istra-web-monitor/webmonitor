from ShellUtils.Shellathon import *
from Logathon import logathon

from Executor import Executor, TaskStatus, TupleListCommandsType, TaskInfo, TaskNameType
from ExecUtils import get_ssh_cmd, get_scp_cmd
from ProdServersUtils import node_to_server, nodes_to_servers

from ConfigUtils import Holder
from TypeUtils import senum, GenVar

from configs.prod_servers import PROD_SERVERS, ATTRS_OF_NODE, ServerAttrs, NodeAttrs
from configs.paths import PROD_SCRIPTS_PATH
from configs.regions import REGIONS
from configs.monitor import MONITOR_SERVERS

import datetime, sys, pipes, functools
from typing import Callable, Iterable, Dict, Any, Optional as tOptional, Set, List, Mapping, Tuple, NamedTuple

import json, tempfile, subprocess
from ShellUtils.ProgressBar import ProgressBar

from collections import defaultdict

logger = logathon.register_module()
executors = []
all_servers = PROD_SERVERS.copy()
all_servers.update(MONITOR_SERVERS)

RunnerType = senum("JSON", "USER", "OUTPUT_FILE")
configuration = Holder(simulation=True, runner=RunnerType.JSON, region="All")

class SchedDayRunCommand(NamedTuple):
    func: Callable
    kwargs: Dict[str, Any]
    override_nodes: bool

OMSERVER_ACTIONS = ["suppress", "unsuppress", "purge"]
OMSERVER_FLAGS = dict(
    brief="brief-csv",
    positions="positions",
    orders="orders",
    inflight="orders",
    rejected="rejected",
    slow="slow",
    limbo="limbo",
    suppressed="suppressed")

RequiredFilesType = tOptional[Iterable[Tuple[str,str]]]

class InterventorInputError(Exception):
    pass

class NodesValidationError(Exception):
    pass

class ServersValidationError(Exception):
    pass

def true_func(*args, **kwargs) -> bool: 
    return True

def chunks(lst: List[GenVar], n: int) -> Iterable[List[GenVar]]:
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def validate_configuration() -> None:
    global configuration
    global all_servers
    if configuration.region != "All":
        if configuration.region not in REGIONS:
            raise InterventorInputError("Region %s doesn't exist" % (configuration.region, ))
        all_servers = { key : server for key, server in all_servers.items() if server.region == REGIONS[configuration.region] }
        

def validate_servers(servers, server_validation_lambda: Callable[[ServerAttrs], bool]):
    missing_servers = [server_id for server_id in servers if server_id not in all_servers]
    if missing_servers:
        raise ServersValidationError("Not all servers exist in current configuration (wrong region?): %r" % (missing_servers,))
    invalid_servers = [server_id for server_id in servers if not server_validation_lambda(all_servers[server_id])]
    if invalid_servers:
        raise ServersValidationError("Some servers didn't pass validation: %r" % (invalid_servers,))


def validate_nodes(nodes: Iterable[int], node_validation_lambda: Callable[[NodeAttrs], bool]):
    missing_nodes = [node for node in nodes if node_to_server(node) not in all_servers or node not in all_servers[node_to_server(node)].nodes]
    if missing_nodes:
        raise NodesValidationError("Not all nodes exist (wrong region?): %r" % (missing_nodes, ))
    invalid_nodes = [node for node in nodes if not node_validation_lambda(ATTRS_OF_NODE(node))]
    if invalid_nodes:
        raise NodesValidationError("Some nodes didn't pass validation: %r" % (invalid_nodes, ))


def sched_day_run_command(func: Callable, kwargs: Dict[str, Any], nodes: tOptional[Iterable[int]] = None, override_func_nodes: bool = False) -> str:
    if override_func_nodes:
        kwargs.update(nodes=nodes)
    return Scmd(module_dirpath = PROD_SCRIPTS_PATH,
                func = func, 
                func_kws = kwargs)


def outfile_runner(named_commands: TupleListCommandsType, valid_retcodes: Set[int], reverse_result: bool) -> int:
    if (reverse_result):
        valid_retcodes = set(list(range(256))) - valid_retcodes

    def on_update(name: TaskNameType, task_info: TaskInfo, stdout: str, stderr: str):
        if reverse_result and task_info.status != TaskStatus.Running:
            task_info.status = TaskStatus.Error if task_info.status in {TaskStatus.Success, TaskStatus.Warning} else TaskStatus.Success 
    executor = Executor(named_commands, valid_retcodes=valid_retcodes, on_update_callback=on_update)
    executors.append(executor)
    executor.run_sync()

    names = [name for name, commands in named_commands]
    results = [ [executor.tasks[n].stdout, executor.tasks[n].stderr, executor.tasks[n].return_code, executor.tasks[n].name] for n in names]
    out = tempfile.NamedTemporaryFile(delete=False, mode="w+t")
    out.write(json.dumps(results))
    out.write("\n")
    out.close()
    print(out.name, file=sys.stderr)

    return 0 if all([t.status in {TaskStatus.Success, TaskStatus.Warning} for t in executor.tasks.values()]) else 2


class json_runner:
    @staticmethod
    def on_start(name, task_info):
        logger.info("%s: started command (pid: %d): %s" % (name, task_info.process.pid, task_info.command))
    @staticmethod
    def on_update(name: TaskNameType, task_info: TaskInfo, stdout: str, stderr: str, reverse_result: bool = False):
        if reverse_result and task_info.status != TaskStatus.Running:
            task_info.status = TaskStatus.Error if task_info.status in {TaskStatus.Success, TaskStatus.Warning} else TaskStatus.Success 
        result_dict = dict(server=name, return_code=task_info.return_code, status=task_info.status, stdout=stdout or "", stderr=stderr or "")
        sys.stdout.write(json.dumps(result_dict) + "\n")
        sys.stdout.flush()
        if stdout:
            logger.info(f"{name}: STDOUT:\n{stdout}")
        if stderr:
            logger.info(f"{name}: STDERR:\n{stderr}")
        if task_info.status == TaskStatus.Running and task_info.return_code is not None:
            logger.info("%s: running next command: %s" % (name, task_info.commands[task_info.index+1]))
    @staticmethod
    def on_completion(name: TaskNameType, task_info: TaskInfo):
        logger.info(f"Task {name} completed. return_code={task_info.return_code}, status={task_info.status}")
    @staticmethod
    def runner(named_commands: TupleListCommandsType, valid_retcodes: Set[int], reverse_result: bool) -> int:
        if (reverse_result):
            valid_retcodes = set(list(range(256))) - valid_retcodes
        executor = Executor(named_commands, valid_retcodes=valid_retcodes, on_started_callback=json_runner.on_start,
                            on_update_callback=functools.partial(json_runner.on_update,
                                                                 reverse_result=reverse_result),
                            on_completion_callback=json_runner.on_completion
        )
        executors.append(executor)

        logger.info("Start running...")
        executor.run_sync()
        logger.info("Finished.")

        return 0 if all([t.status in {TaskStatus.Success, TaskStatus.Warning} for t in executor.tasks.values()]) else 2


def pb_runner(named_commands: TupleListCommandsType, valid_retcodes: Set[int], reverse_result: bool) -> int:
    def on_start(name: TaskNameType, task_info: TaskInfo):
        pb.update(total, f"Task {name} Started")

    def on_update(name: TaskNameType, task_info: TaskInfo, stdout: str, stderr: str):
        nonlocal total #type: ignore
        if task_info.return_code is not None:
            total += 1 / len(task_info.commands)
        if reverse_result and task_info.status != TaskStatus.Running:
            task_info.status = TaskStatus.Error if task_info.status in {TaskStatus.Success, TaskStatus.Warning} else TaskStatus.Success 
        if stdout or stderr or task_info.return_code is not None:
            pb.update(total, f"{name}: OUT: {stdout} | ERR: {stderr}")

    def on_completion(name: TaskNameType, task_info: TaskInfo):
        pb.update(total, f"Task {name} completed: {task_info.status}")

    pb = ProgressBar(len(named_commands))
    total: float = 0

    if (reverse_result):
        valid_retcodes = set(list(range(256))) - valid_retcodes

    executor = Executor(named_commands, valid_retcodes=valid_retcodes, on_started_callback=on_start, 
                        on_update_callback=on_update, on_completion_callback=on_completion)
    executors.append(executor)
    executor.run_sync()

    pb.finish()

    for name, _command in named_commands:
        ti = executor.tasks[name]
        print("*" * 50)
        print(f"Task {name} : {ti.status}")
        print("*" * 50)
        print("STDOUT:")
        print("=" * 50)
        print(ti.stdout)
        print("=" * 50)
        print("STDERR:")
        print("=" * 50)
        print(ti.stderr)

    return 0 if all([t.status in {TaskStatus.Success, TaskStatus.Warning} for t in executor.tasks.values()]) else 2

def wrap_with_echo(per_server_commands: TupleListCommandsType, required_files: RequiredFilesType, ssh_flags="") -> TupleListCommandsType:
    file_commands = defaultdict(list)
    if required_files:
        for server, _commands in per_server_commands:
            srv = PROD_SERVERS[server]
            file_commands[server] = ["echo {}".format(pipes.quote(get_scp_cmd(src, dst, dst_host=srv.hostname, dst_user=srv.username))) for src, dst in required_files]
    return [(server, file_commands[server] + ["echo {}".format(pipes.quote(get_ssh_cmd(cmd, all_servers[server].hostname, all_servers[server].username, extra_ssh_flags=ssh_flags))) for cmd in commands]) for server, commands in per_server_commands]


def wrap_with_ssh(per_server_commands: TupleListCommandsType, required_files: RequiredFilesType, ssh_flags: str = "") -> TupleListCommandsType:
    file_commands = defaultdict(list)
    if required_files:
        for server, _commands in per_server_commands:
            srv = all_servers[server]
            file_commands[server] = [get_scp_cmd(src, dst, dst_host=srv.hostname, dst_user=srv.username) for src, dst in required_files]
    return [(server, file_commands[server] + [get_ssh_cmd(cmd, all_servers[server].hostname, all_servers[server].username, extra_ssh_flags=ssh_flags) for cmd in commands]) for server, commands in per_server_commands]


def get_wrapped_commands(per_server_commands: TupleListCommandsType, required_files: RequiredFilesType, ssh_flags="") -> TupleListCommandsType:
    wrapper = wrap_with_echo if configuration.simulation else wrap_with_ssh
    return wrapper(per_server_commands, required_files, ssh_flags=ssh_flags)


def run_full_commands(per_server_full_commands: TupleListCommandsType, valid_retcodes: Set[int] = {0}, reverse_result: bool = False) -> int:
    runners = {
        RunnerType.JSON: json_runner.runner,
        RunnerType.USER: pb_runner,
        RunnerType.OUTPUT_FILE: outfile_runner
    }
    actual_runner = runners.get(configuration.runner, json_runner.runner)
    return actual_runner(per_server_full_commands, valid_retcodes, reverse_result)

def run_commands_serially_until_success(commands: Iterable[str], shell: bool = True) -> int:
    for command in commands:
        try:
            subprocess.check_call(command, shell=shell)
            return 0
        except subprocess.CalledProcessError:
            pass
    logger.error("Could not send command to any server.")
    return 2

def runner(per_server_commands: TupleListCommandsType, required_files: RequiredFilesType, ssh_flags="") -> int:
    return run_full_commands(get_wrapped_commands(per_server_commands, required_files, ssh_flags=ssh_flags))


def run_sched_day_run(func: Callable, kwargs: Dict[str, Any], servers=None, nodes=None, 
                      node_validation_lambda=true_func, 
                      server_validation_lambda=true_func,
                      override_func_nodes=None, required_files=None):
    return run_multi_sched_day_run([SchedDayRunCommand(func, kwargs, override_func_nodes)], 
                                   servers, nodes, node_validation_lambda, 
                                   server_validation_lambda, required_files)


def run_multi_sched_day_run(funcs_and_kwargs: Iterable[SchedDayRunCommand], servers: tOptional[Iterable[ServerAttrs]] = None, nodes: tOptional[Iterable[int]] = None, 
                            node_validation_lambda: Callable[[NodeAttrs], bool] = true_func, 
                            server_validation_lambda: Callable[[ServerAttrs], bool] = true_func,
                            required_files: RequiredFilesType = None) -> int:

    validate_configuration()

    if nodes and servers:        
        raise InterventorInputError("Only nodes or servers must be given")
    if not nodes and not servers:        
        raise InterventorInputError("Either nodes or servers must be present for this to work")

    actual_servers: Set[int] = set()

    if nodes:
        override_func_nodes = True
        validate_nodes(nodes, node_validation_lambda)
        per_server_nodes: Mapping[int, tOptional[List[int]]] = nodes_to_servers(nodes)
        actual_servers.update(node_to_server(node) for node in nodes)        

    if servers:
        override_func_nodes = False
        per_server_nodes = {server.server_id: None for server in servers}
        actual_servers.update(server.server_id for server in servers)

    validate_servers(actual_servers, server_validation_lambda)
    
    per_server_commands = [
        (server,
         [sched_day_run_command(
                func=func, 
                kwargs=kwargs, 
                nodes=nodes,
                override_func_nodes=(override_func_nodes if override_nodes is None else override_nodes))
            for func, kwargs, override_nodes in funcs_and_kwargs]
        )
        for server, nodes in per_server_nodes.items()
        ]

    return run_per_server_commands(per_server_commands, server_validation_lambda, required_files)


def run_servers_command(command: str, servers: tOptional[Iterable[ServerAttrs]] = None, server_validation_lambda: Callable[[ServerAttrs], bool] = true_func, required_files: RequiredFilesType = None, ssh_flags="") -> int:
    return run_servers_commands([command], servers, server_validation_lambda, required_files, ssh_flags=ssh_flags)


def run_servers_commands(commands: Iterable[str] = [], servers: tOptional[Iterable[ServerAttrs]] = None, server_validation_lambda: Callable[[ServerAttrs], bool] = true_func, required_files: RequiredFilesType = None, ssh_flags="") -> int:
    if servers is None:
        raise Exception("Cannot accept None for servers")
    per_server_commands = [(getattr(server, "server_id", server.hostname), list(commands)) for server in servers]
    return run_per_server_commands(per_server_commands, server_validation_lambda, required_files, ssh_flags=ssh_flags)


def run_per_server_commands(per_server_commands: TupleListCommandsType, server_validation_lambda: Callable[[ServerAttrs], bool] = true_func, required_files: RequiredFilesType = None, ssh_flags="") -> int:
    validate_configuration()
    servers = {server for server, _commands in per_server_commands }
    validate_servers(servers, server_validation_lambda)

    return runner(per_server_commands, required_files, ssh_flags=ssh_flags)
