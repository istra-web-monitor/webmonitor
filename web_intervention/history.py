#pylint: disable=unexpected-keyword-arg
from elasticsearch import Elasticsearch
from pydantic import BaseModel
import datetime, calendar
from typing import List, Mapping, Any

class HistoryItem(BaseModel):
    user: str
    summary: str = ""
    command_summary: str
    details: str
    region: str
    result: str
    state: str
    id: str
    timestamp: datetime.datetime = datetime.datetime.now()

_SUMMARY_EXCLUDED_FIELDS = ["command_summary", "details", "result"]
def _to_unix_time(dt: datetime.datetime) -> int:
    return calendar.timegm(dt.utctimetuple())

def to_hist(r: Mapping[str, Any]) -> HistoryItem:
    d = r["_source"]
    for field in _SUMMARY_EXCLUDED_FIELDS:
        d.setdefault(field, "")

    if "id" in d:
        return HistoryItem(**d)

    return HistoryItem(**dict(id=r["_id"], **d))

class History:
    def __init__(self) -> None:
        self._es = None
        self.index = "web-intervention-history"

    @property
    def es(self):
        if self._es is None:
            for es_host in ("localhost", "elastic-master"):
                try:
                    es = Elasticsearch(hosts=[{"host": es_host, "port": 9200}])
                    if es.ping():
                        self._es = es
                        break
                except Exception:
                    pass
            else:
                raise Exception(f"Failed to communicate with Elasticsearch for all servers")

        return self._es

    def add(self, data: HistoryItem):
        data.timestamp = _to_unix_time(data.timestamp) #type: ignore[assignment]
        self.es.create(index=self.index, body=data.json(), refresh=True, id=data.id)

    def update(self, id: str, data: HistoryItem):
        data.timestamp = _to_unix_time(data.timestamp) #type: ignore[assignment]
        self.es.update(index=self.index, body=f'{{"doc":{data.json()}}}', refresh=True, id=id)

    def get(self,
            region: str,
            start_time: datetime.datetime,
            end_time: datetime.datetime) -> List[HistoryItem]:
        start_time = _to_unix_time(start_time) #type: ignore[assignment]
        end_time = _to_unix_time(end_time) #type: ignore[assignment]
 
        res = self.es.search(
            index=self.index,
            body=dict(
                size=10000,
                query=dict(
                    bool=dict(
                        must=[
                            dict(match=dict(region=region)),
                            dict(range=dict(timestamp=dict(gte=start_time, lte=end_time))),
                        ]
                    )
                ),
                _source=dict(excludes=_SUMMARY_EXCLUDED_FIELDS),
            )
        )

        try:
            return [to_hist(r) for r in res["hits"]["hits"]]
        except KeyError:
            return []

    def get_by_id(self,
                  region: str,
                  id: str) -> HistoryItem:
 
        res = self.es.search(
            index=self.index,
            terminate_after=1,
            body=dict(
                size=10000,
                query=dict(
                    bool=dict(
                        must=[
                            dict(match=dict(region=region)),
                            dict(match=dict(_id=id)),
                        ]
                    )
                )
            )
        )

        if res["hits"]["total"]["value"] != 1:
            raise Exception(f"ID {id} not found.")
        return to_hist(res["hits"]["hits"][0])

