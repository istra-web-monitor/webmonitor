#!/usr/istra/bin/pypy3

from ShellUtils.Shellathon import *
from Logathon import logathon
from configs.prod_servers import AProdServer, ARecordingServer, server_types
from configs.monitor import self_monitor_server, get_region_shob_monitor_server
import ProcessUtils, TimeUtils, os

from web_intervention.interventor_utils import *
import web_intervention.interventor_utils as interventor_utils
import sched_day_run
from sched_day_run import ANodeSet, node_roles
from configs.monitor import ARLEServer, AMonitorServer
from configs.paths import PROD_SCRIPTS_PATH
from configs.exchanges import AExchange
from configs.regions import ARegion, REGIONS
from configs.feeds import AFeed
from configs.sched_names import ASchedName
from configs.prod_servers import PROD_SERVERS, ANodeAttr, PRODUCTION_SERVERS, WITH_ISTRA_MH_SERVERS
from ProdServersUtils import node_to_server
from RUtils import R_read_csv, R_read_vector, run_R_cmds, boolean_to_r_boolean
import RServer
from Executor import TaskStatus, TaskInfo

from ConfigUtils import Holder
from collections import defaultdict, Counter

from web_intervention.remote_commands_patchy import *
from TimeUtils import format_date, currtime_formatted, hour2ms

from subprocess import PIPE
import sys, json, random, tempfile, csv, io, re, socket, bisect, math, subprocess

logger = logathon.register_module()

SERVERS_ATYPE = AList(AProdServer("prod servers to send command to. Leave None to be computed from nodes"))
SERVERS_ONLY_ATYPE = AList(AProdServer("prod servers to send command to."))

def get_monitor_server():
    return self_monitor_server() or get_region_shob_monitor_server(REGIONS["America"])

# KILL.FH.NODES
@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.stop_feedhandler_nodes,
    new_defaults=dict(nodes=None, date=None, force_stop=True),
    fixed_defaults=dict(interactive=False)
)
def stop_feedhandler_nodes(servers=None, **kwargs):
    return run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.stop_feedhandler_nodes, kwargs=kwargs)


# KILL.MH.NODES
@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.stop_markethandler_nodes,
    new_defaults=dict(nodes=None, date=None, force_stop=True),
    fixed_defaults=dict(interactive=False)
)
def stop_markethandler_nodes(servers=None, **kwargs):
    return run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.stop_markethandler_nodes, kwargs=kwargs)


# KILL.TRADING.NODES
@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.stop_trading_nodes,
    new_defaults=dict(nodes=None, date=None, force_stop=True),
    fixed_defaults=dict(interactive=False)
)
def stop_trading_nodes(servers=None, **kwargs):
    return run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.stop_trading_nodes, kwargs=kwargs)


# KILL.RECORDING.NODES
@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.stop_recording_nodes,
    new_defaults=dict(nodes=None, date=None, force_stop=True),
    fixed_defaults=dict(interactive=False)
)
def stop_recording_nodes(servers=None, **kwargs):
    return run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.stop_recording_nodes, kwargs=kwargs)

# LIQUIDIATE
@cmdline(
    AList(ATuple("Node to Symbols pair",
                 ANodeAttr("Node"),
                 AList(AString("Symbol")))),
                 AOneOf(AString("Symbols suspension state"), ["ALL", "ONLY_SUSPENDED", "ONLY_NOT_SUSPENDED"]))
def liquidate(node_symbols, symbols_suspension_state):
    per_server = defaultdict(list)
    for node, symbols in node_symbols:
        per_server[node.server.server_id].append((node, symbols))
    
    # TODO - pass symbols_suspension_state
    result = run_per_server_commands([(server_id,
                                       [sched_day_run_command(
                                            func=sched_day_run.send_rcmd,
                                            kwargs=dict(rcmd=rc_manual_liquidate(symbols),
                                                        b_rcmd_output=True,
                                                        nodes=[node.node_id])) for node, symbols in node_symbols
                                       ])
                                       for server_id, node_symbols in per_server.items()])

    return result


# START.TRADING.NODES
@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.start_trading_nodes,
    new_defaults=dict(nodes=None, date=None),
    fixed_defaults=dict(interactive=False)
)
def start_trading_nodes(servers=None, **kwargs):
    if kwargs.get("fastness", sched_day_run.FASTNESS.all) == sched_day_run.FASTNESS.all:
        if servers:
            raise NotImplementedError("Not implemented")
            # result = run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.start_trading_nodes, kwargs=dict(kwargs, fastness=sched_day_run.FASTNESS.fast))
            # result += run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.start_trading_nodes, kwargs=dict(kwargs, fastness=sched_day_run.FASTNESS.slow))
            # return result
        nodes = kwargs["nodes"]
        fast_nodes = [n for n in nodes if ATTRS_OF_NODE(n).is_fast_trader]
        slow_nodes = list(set(nodes)-set(fast_nodes))
        server_to_nodes=defaultdict(lambda: defaultdict(list))
        for node in fast_nodes:
            server_to_nodes[node_to_server(node)][sched_day_run.FASTNESS.fast].append(node)
        for node in slow_nodes:
            server_to_nodes[node_to_server(node)][sched_day_run.FASTNESS.slow].append(node)

        per_server_commands = [
            (server,
            [sched_day_run_command(
                    func=sched_day_run.start_trading_nodes, 
                    kwargs=kwargs, 
                    nodes=current_nodes,
                    override_func_nodes=True)
                for fast_or_slow, current_nodes in sorted(fast_or_slow_obj.items(), key=lambda k: 0 if k[0] == sched_day_run.FASTNESS.fast else 1)]
            )
            for server, fast_or_slow_obj in server_to_nodes.items()
            ]

        return run_per_server_commands(per_server_commands)

    else:
        return run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.start_trading_nodes, kwargs=kwargs)

# START.NODES
@cmdline(
    SERVERS_ATYPE,
    AList(ANodeAttr("Nodes to affect")),
    AOneOf(AString("The type of variant you want to run now"), sched_day_run.VARIANT_TYPES),
    AString("The name of the variant to run. Only valid if variant_type is specific"),
    AOneOf(AString("Running the nodes in either simulation or production mode"),
           sched_day_run.RUNNING_MODES),
    ABool("If true, asks for the user approval in critical junctions"),
    ABool("If true will stop and start the nodes. If false will start only the already stopped nodes"),
    ABool("If true, don't request confirmation for warnings"),
)
def start_nodes(servers=None,
                nodes=None,
                variant_type='auto',
                variant_name=None,
                running_mode='prod',
                interactive=False,
                force_restart=False,
                bypass_warnings=False):
    if variant_name == "":
        variant_name = None

    roles = list({n.role for n in nodes})
    if len(roles) != 1:
        raise Exception(f"Only one node type is allowed. Got: {roles}")
    node_role = roles[0]
    if node_role == node_roles.TRADING:
        f = start_trading_nodes
    elif node_role == node_roles.ISTRA_MARKET_HANDLER:
        f = start_markethandler_nodes
    elif node_role == node_roles.FEED_HANDLER:
        f = start_feedhandler_nodes
    elif node_role == node_roles.RECORDING:
        f = start_recording_nodes
    else:
        raise Exception(f"Unexpected node role {node_role}")

    return f(servers=servers, nodes=[n.node_id for n in nodes],
             variant_type=variant_type,
             variant_name=variant_name,
             running_mode=running_mode,
             interactive=interactive,
             force_restart=force_restart,
             bypass_warnings=bypass_warnings)


@cmdline(
    SERVERS_ATYPE,
    AList(ANodeAttr("Nodes to affect")))
def kill_nodes(servers=None,
               nodes=None):
    roles = list({n.role for n in nodes})
    if len(roles) != 1:
        raise Exception(f"Only one node type is allowed. Got: {node_roles}")
    node_role = roles[0]
    if node_role == node_roles.TRADING:
        f = stop_trading_nodes
    elif node_role == node_roles.ISTRA_MARKET_HANDLER:
        f = stop_markethandler_nodes
    elif node_role == node_roles.FEED_HANDLER:
        f = stop_feedhandler_nodes
    elif node_role == node_roles.RECORDING:
        f = stop_recording_nodes
    else:
        raise Exception(f"Unexpected node role {node_role}")

    return f(servers=servers, nodes=[n.node_id for n in nodes], force_stop=True)

# START.LMM.QUOTERS.NODES
@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.start_trading_nodes,
    new_defaults=dict(nodes=None, date=None),
    fixed_defaults=dict(interactive=False)
)
def start_lmm_quoters_nodes(servers=None, **kwargs):
    "Start the desired LMM quoters nodes with the desired variants"
    return run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.start_trading_nodes, kwargs=kwargs, 
                             node_validation_lambda=lambda node_attrs: node_attrs.is_quoter or node_attrs.is_limper)


# START.PERIPHERALS
@cmdline_extend(
    SERVERS_ONLY_ATYPE,
    from_func=sched_day_run.start_peripherals,
    new_defaults=dict(date=None, start_feedhandlers=True, start_markethandlers=True, start_fast_trading_nodes=True),
    fixed_defaults=dict(interactive=False)
)
def start_peripherals(servers, **kwargs):
    logger.info("Starting services")
    result = run_sched_day_run(servers=servers, func=sched_day_run.start_peripherals, kwargs=dict(start_feedhandlers=False, start_markethandlers=False, start_fast_trading_nodes=False))

    logger.info("Starting peripherals")
    fh_nodes = [n.node_id for s in servers for n in s.nodes.values() if n.role == node_roles.FEED_HANDLER]
    ld4_compacted_senders_nodes, lse_compacted_fhs_nodes = get_ld4_and_lse_compacted_senders(fh_nodes)

    if fh_nodes and ld4_compacted_senders_nodes and lse_compacted_fhs_nodes:
        lse_compacted_fhs_servers = [s for s in servers
                                     if any(n in s.nodes.keys() for n in lse_compacted_fhs_nodes)]
        logger.info(f"LSE sender and compacted servers: {', '.join([str(s.server_id) for s in lse_compacted_fhs_servers])}")
        logger.info("Starting non-LSE compacted servers")
        result = run_sched_day_run(servers=list(set(servers) - set(lse_compacted_fhs_servers)), func=sched_day_run.start_peripherals, kwargs=kwargs)
        logger.info("starting remaining LSE compacted FHs")
        result += run_sched_day_run(servers=lse_compacted_fhs_servers, func=sched_day_run.start_peripherals, kwargs=kwargs)
    else:
        result += run_sched_day_run(servers=servers, func=sched_day_run.start_peripherals, kwargs=kwargs)

    return result


# KILL.PERIPHERALS
@cmdline_extend(
    SERVERS_ONLY_ATYPE,
    from_func=sched_day_run.kill_regular_peripherals,
    new_defaults=dict(date=None),
    fixed_defaults=dict(interactive=False)
)
def kill_services(servers, **kwargs):
    return run_sched_day_run(servers=servers, func=sched_day_run.kill_regular_peripherals, kwargs=kwargs)

# START.PERIPHERALS
@cmdline_extend(
    SERVERS_ONLY_ATYPE,
    from_func=sched_day_run.start_peripherals,
    new_defaults=dict(date=None),
    fixed_defaults=dict(interactive=False, start_feedhandlers=False, start_markethandlers=False, start_fast_trading_nodes=False)
)
def start_services(servers, **kwargs):
    return run_sched_day_run(servers=servers, func=sched_day_run.start_peripherals, kwargs=kwargs)

def create_R_server(hide_stdout=True):
    json_output = False
    if interventor_utils.configuration.runner == interventor_utils.RunnerType.JSON:
        json_output = True
    server = RServer.RServer(hide_stdout=hide_stdout, allow_output_callbacks=json_output,
                             remote_server="mgmtsrv" if "shob" in socket.gethostname() else None)
    json_task = None
    def on_json_update(out, err, returncode):
        complete = False
        if returncode is not None:
            if json_task.return_code is None:
                complete = True
                json_task.return_code = returncode
                if returncode == 0:
                    json_task.status=TaskStatus.Success
                else:
                    json_task.status=TaskStatus.Error
        interventor_utils.json_runner.on_update(json_task.name, json_task, out, err)

        if complete:
            interventor_utils.json_runner.on_completion(json_task.name, json_task)

    if json_output:
        json_task = TaskInfo(name="R Command",
                                      status=TaskStatus.Running,
                                      process = server.sp) #somewhat intrusive
        interventor_utils.json_runner.on_start(json_task.name, json_task)
        server.set_callback(on_json_update)

    if interventor_utils.configuration.simulation:
        server.run(["""set.manual.trader.mode("returnOnly")"""])

    return server

_lse_compacted_fhs_nodes = None
def get_lse_compacted_fhs_nodes():
    global _lse_compacted_fhs_nodes
    if _lse_compacted_fhs_nodes is None:
        with create_R_server():
            _lse_compacted_fhs_nodes = R_read_vector(cmds=f"""lse.compacted.fhs()""")
    return _lse_compacted_fhs_nodes

def get_ld4_and_lse_compacted_senders(nodes): 
    ld4_compacted_senders_nodes = [n.node_id
            for s in PROD_SERVERS.values() if s.site.name == "LD4"
            for n in s.nodes.values() if n.node_id in nodes and n.is_feed_sender and n.is_active]

    if ld4_compacted_senders_nodes:
        lse_compacted_fhs_nodes = [n for n in nodes if n in get_lse_compacted_fhs_nodes()]
    else:
        lse_compacted_fhs_nodes = None  # not relevant, and slow to compute
    
    return (ld4_compacted_senders_nodes, lse_compacted_fhs_nodes)

# START.FH.NODES
@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.start_feedhandler_nodes,
    new_defaults=dict(nodes=None, date=None),
    fixed_defaults=dict(interactive=False)
)
def start_feedhandler_nodes(servers=None, **kwargs):
    nodes = kwargs["nodes"]
    ld4_compacted_senders_nodes, lse_compacted_fhs_nodes = get_ld4_and_lse_compacted_senders(nodes)

    if nodes and ld4_compacted_senders_nodes and lse_compacted_fhs_nodes:
        logger.info(f"LD4/LSE sender and compacted nodes: {', '.join([str(n) for n in ld4_compacted_senders_nodes])} / {', '.join([str(n) for n in lse_compacted_fhs_nodes])}")
        logger.info("Starting independent FHs and LD4 compacted senders")
        result = run_sched_day_run(servers=servers, nodes=list(set(nodes) - set(lse_compacted_fhs_nodes)), func=sched_day_run.start_feedhandler_nodes, kwargs=kwargs)
        logger.info("starting LSE compacted FHs")
        result += run_sched_day_run(servers=servers, nodes=lse_compacted_fhs_nodes, func=sched_day_run.start_feedhandler_nodes, kwargs=kwargs)
        return result
    else:
        return run_sched_day_run(servers=servers, nodes=nodes, func=sched_day_run.start_feedhandler_nodes, kwargs=kwargs)


# START.MH.NODES
@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.start_markethandler_nodes,
    new_defaults=dict(nodes=None, date=None),
    fixed_defaults=dict(interactive=False)
)
def start_markethandler_nodes(servers=None, **kwargs):
    return run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.start_markethandler_nodes, kwargs=kwargs)


# START.RECORDING.NODES
@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.start_recording_nodes,
    new_defaults=dict(nodes=None, date=None),
    fixed_defaults=dict(interactive=False)
)
def start_recording_nodes(servers=None, **kwargs):
    return run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.start_recording_nodes, kwargs=kwargs)


# START.BOOK.SUPPLIER.NODES
@cmdline_extend(
    SERVERS_ONLY_ATYPE,
    from_func=sched_day_run.start_booksupplier_nodes,
    new_defaults=dict(date=None),
    fixed_defaults=dict(interactive=False)
)
def start_booksupplier_nodes(servers, **kwargs):
    return run_sched_day_run(servers=servers, func=sched_day_run.start_booksupplier_nodes, kwargs=kwargs)

# SET.STRATEGY / SET.SYMBOL
@cmdline_extend(
    ANodeSet(None, "Nodes to affect"),
    from_func=sched_day_run.exclusion_symbols,
    new_defaults=dict(date=None),
)
def exclusion_symbols(nodes, **kwargs):
    if kwargs["strats"] == ["@all"]:
        kwargs["strats"] = []
    exclusion_symbols_command = SchedDayRunCommand(sched_day_run.exclusion_symbols, kwargs, override_nodes=False)
    remote_command = SchedDayRunCommand(
        sched_day_run.send_rcmd,
        dict(
            rcmd=rc_set_strats_exclusion(
                symbols=kwargs["symbols"],
                reason=kwargs["reason"],
                strategies=kwargs["strats"],
                bExclude=kwargs["action"] == "exclude"),
            b_rcmd_output=True),
        override_nodes=True)
    return run_multi_sched_day_run([exclusion_symbols_command, remote_command], nodes=nodes)


# EXIT.GRACEFULLY 
@cmdline(
    AOneOf(AString("Action"), ["exit", "resume"]),
    AList(ATuple("Node to Symbols pair",
                 ANodeAttr("Node"),
                 AList(AString("Symbol")))),
    AFromFun(sched_day_run.send_rcmd, "persistent")
)
def exit_gracefully(action, node_symbols, persistent):
    per_server = defaultdict(list)
    for node, symbols in node_symbols:
        per_server[node.server.server_id].append((node, symbols))

    time = hour2ms(22) if (action == "exit") else hour2ms(24)
    
    result = run_per_server_commands([(server_id,
                                       [sched_day_run_command(
                                           func=sched_day_run.send_rcmd,
                                           kwargs=dict(rcmd=rc_set_symbols_exit_time(symbols, time),
                                                       b_rcmd_output=True,
                                                       persistent=persistent,
                                                       nodes=[node.node_id])) for node, symbols in node_symbols
                                       ])
                                      for server_id, node_symbols in per_server.items()])
    
    return result   


# GET.SYMBOL.EXCLUSION.STATUS
@cmdline_extend(
    SERVERS_ONLY_ATYPE,
    from_func=sched_day_run.exclusion_symbols_status,
    fixed_defaults=dict(json_format=True, file_path=None, simulated=False),
)
def get_symbols_exclusion_status(servers, **kwargs):
    command = Scmd(module_dirpath=PROD_SCRIPTS_PATH,
                   func=sched_day_run.exclusion_symbols_status,
                   func_kws=kwargs)

    run_servers_command(command=command, servers=servers)
    rv = []
    for task in executors[0].tasks.values():
        if task.status == TaskStatus.Success:
            data = json.loads(task.stdout)
            for row in data:
                row['server'] = task.name
            rv += data

    result = [dict(type="table", data=rv)]
    print(json.dumps(result))


# MODIFY.ORDER.STATE.IN.OMSERVER.BY
@cmdline(
    AOneOf(AString("omserver action"), OMSERVER_ACTIONS),
    AList(ANodeAttr("Nodes to affect")),
    ANoneOr(AList(AExchange("Exchanges to filter. None for all"))),
    ANoneOr(AList(AInt("Ports to filter. None for all"))),
    ABool("Only limbo"),
    AInt("Orders limit. Error will be thrown if there are more"),
    ANoneOr(AString("get_omserver_summary file")),
    ANoneOr(AString("File to output csv to")),
    ABool("Print result csv to stdout"),
    ANoneOr(AList(AProdServer("MH servers to check for limbo orders. Leave None for all MH servers in sched name."))),
)
def modify_order_state_in_omserver_by(action, nodes, exchanges=None, ports=None, only_limbo=True, orders_limit=2000, omserver_filename=None, filename=None, print_result=True, mh_servers=None):
    "Modify order state in omserver process by query conditions. Default is suppress. Limited number of orders for safety measures"

    if mh_servers is None:
        sched_names = list({n.server.sched_name for n in nodes})
        if len(sched_names) == 0:
            raise Exception("Unknown error - no sched name for any node. Maybe empty list of nodes??")
        if len(sched_names) > 1:
            raise Exception(f"Multiple sched names not supported. Got: {[s.name for s in sched_names]}")
        sched_name = sched_names[0]
        mh_servers = [s for s in WITH_ISTRA_MH_SERVERS if s.sched_name.name == sched_name.name and s.server_type == server_types.PRODUCTION]
    nodes = [n.node_id for n in nodes]
    if omserver_filename:
        with open(omserver_filename) as csv_file:
            data = list(csv.DictReader(csv_file))
    else:
        mode = "limbo" if only_limbo else "orders"
        data = get_omserver_summary_csv_reader(servers=mh_servers, nodes=nodes, mode=mode)

    if exchanges:
        exchanges = {exchange.istra_mark for exchange in exchanges}
    nodes = set(nodes)

    norders = 0
    servers_to_orderids = defaultdict(list)
    for row in data:
        if only_limbo and int(row["IsLimbo"]) != 1:
            continue
        if exchanges and row["Exchange"] not in exchanges:
            continue
        if ports and int(row["PortIdx"]) not in ports:
            continue
        if nodes and int(row["Node"]) not in nodes:
            continue
        norders += 1
        servers_to_orderids[PROD_SERVERS[node_to_server(row["Node"])]].append(row["OrderId"])

    if norders > orders_limit:
        raise InterventorInputError(f"Orders limit breached. Limit: {orders_limit} Requested: {norders}")

    logger.info(f"Found {norders} orders to {action}")

    if norders > 0:
        return modify_order_state_in_omserver(list(servers_to_orderids.items()), action, filename, print_result)
    return 0


# MODIFY.ORDER.STATE.IN.OMSERVER
@cmdline(
    AList(ATuple("MH Server - Order IDs", AProdServer("MH Server"), AList(AString("OrderID")))),
    AOneOf(AString("omserver action"), OMSERVER_ACTIONS),
    ANoneOr(AString("File to output csv to")),
    ABool("Print result csv to stdout"),
)
def modify_order_state_in_omserver(server_to_orderids, action, filename=None, print_result=True):
    "Modify order state in omserver process, by server and order id."
    command = f"{PROD_SCRIPTS_PATH}/omserver.py {action}"
    result = run_per_server_commands([(server.server_id,
                                       ["{} {}".format(command, ",".join(orders)),
                                        sched_day_run_command(
                                            func=sched_day_run.send_mh_rcmd,
                                            kwargs=dict(rcmd=rc_modify_romg_orders(orders, action),
                                                        b_rcmd_output=True))
                                        ])
                                       for server, orders in server_to_orderids])

    # Generate result csv
    OrderData = lambda: Holder(mh_server=None, modified=False, mh_modified=False)
    orders = defaultdict(OrderData)
    for task in executors[0].tasks.values():
        for server, order_ids in server_to_orderids:
            if server.server_id != task.name:
                continue
            for order in order_ids:
                orders[order].mh_server = server.server_id
                if f"{order},True" in task.stdout:
                    orders[order].modified = True
                if re.search(f"remote cmd output.*\W{order}\W", task.stderr):
                    orders[order].mh_modified = True

    ios = io.StringIO()
    writer = csv.DictWriter(ios, ["MHServer","OrderId","Modified","MHModified"])
    writer.writeheader()
    for order, data in orders.items():
        writer.writerow(dict(MHServer=data.mh_server, OrderId=order, Modified=data.modified, MHModified=data.mh_modified))

    if print_result:
        text = ios.getvalue()
        if interventor_utils.configuration.runner == interventor_utils.RunnerType.JSON:
            obj = dict(server="result", return_code=0, status=TaskStatus.Success, stdout=text, stderr="")  # a bit hacky - better to use some json printer for both here and to interventor.
            sys.stdout.write(json.dumps(obj))
        else:
            sys.stdout.write(text)

    if filename:
        with open(filename, "wt") as f:
            f.write(ios.getvalue())

    return result


# FEED.EXCLUDE / FEED.INCLUDE
@cmdline_extend(
    AList(AFeed("List of feeds")),
    from_func=sched_day_run.exclusion_feed,
    new_defaults=dict(restart_nodes=True, date=None),
    fixed_defaults=dict(interactive=False)
)
def exclusion_feed(feeds, **kwargs):
    kwargs["feeds"] = list({feed.istra_mark for feed in feeds})
    return run_sched_day_run(nodes=kwargs["nodes"], func=sched_day_run.exclusion_feed, kwargs=kwargs)


# ORDERENTRY.EXCHANGE._
@cmdline_extend(
    AList(AExchange("List of exchanges")),
    from_func=sched_day_run.exclusion_orderentry_exchange,
    new_defaults=dict(restart_nodes=True, date=None),
    fixed_defaults=dict(interactive=False)
)
def exclusion_orderentry_exchange(exchanges, **kwargs):
    "Disallows routing the specified exchanges via the specified MH servers"
    assets = {a for ex in exchanges for a in ex.assets}
    servers = [s for s in PRODUCTION_SERVERS if any(a in assets for a in s.assets)]
    kwargs["exchanges"] = list({exchange.istra_mark for exchange in exchanges})

    return run_sched_day_run(servers=servers, func=sched_day_run.exclusion_orderentry_exchange, kwargs=kwargs)


# ORDERENTRY.LINK._
@cmdline_extend(
    SERVERS_ONLY_ATYPE,
    from_func=sched_day_run.exclusion_orderentry_link,
    new_defaults=dict(restart_nodes=True, date=None),
    fixed_defaults=dict(interactive=False)
)
def exclusion_orderentry_link(servers, **kwargs):
    "Enable link between trading server and MH routing servers"
    return run_sched_day_run(servers=servers, func=sched_day_run.exclusion_orderentry_link, kwargs=kwargs)


# ORDERENTRY.PORT._
@cmdline_extend(
    SERVERS_ONLY_ATYPE,
    from_func=sched_day_run.exclusion_orderentry_port,
    new_defaults=dict(date=None),
    fixed_defaults=dict(interactive=False)
)
def exclusion_orderentry_port(servers, **kwargs):
    return run_sched_day_run(servers=servers, func=sched_day_run.exclusion_orderentry_port, kwargs=kwargs)


# GET.EXCLUSIONS.STATUS
@cmdline_extend(
    SERVERS_ONLY_ATYPE,
    from_func=sched_day_run.exclusion_status,
    new_defaults=dict(date=None),
    fixed_defaults=dict(interactive=False)    
)
def exclusion_status(servers, **kwargs):
    return run_sched_day_run(servers=servers, func=sched_day_run.exclusion_status, kwargs=kwargs)

# PLOT.LMM.LEVELS
@cmdline(
    ADate("Date"),
    AList(AString("Symbol")),
)
def plot_lmm_levels(date, symbols):
    "Plot LMM Levels"
    monitor_server = get_monitor_server().hostname
    with create_R_server():
        reader = R_read_csv(cmds=f"""query.monitor.lmm.levels(date={date.strftime("%Y%m%d")}, symbol=c({",".join('"' + symbol + '"' for symbol in symbols)}), remote.server="{monitor_server}") %>% adjust.market.maker.levels.df()""")
    
    # columns are: time NBBPrice NBOPrice IstraPriceBid IstraPriceAsk
    vals = defaultdict(list)
    for row in reader:
        for col_name, col_value in row.items():
            vals[col_name].append(col_value)

    plot = {
        "type": "plot",
        "title": symbols[0] + " LMM Levels",
        "xlabel": "Time",
        "xtype": "ms",
        "ylabel": "Price",
        "date": date.isoformat(),
        "plots": [{
            "x": vals['time'],
            "y": vals[val],
            "name": val,
        } for val in vals.keys() if val != "time"]
    }

    result = [plot]
    print (json.dumps(result))

    return 0

# PLOT.PNL
@cmdline(
    ADate("Date"),
    AList(AString("Symbol")),
    ANodeSet(None, "Nodes to plot"),
)
def plot_pnl(date, symbols, nodes):
    "Plot PnL"
    monitor_server = get_monitor_server().hostname
    nodes_str = "get.all.trading.nodes()" if not nodes else f"""c({",".join(map(str, nodes))})"""
    with create_R_server():
        reader = R_read_csv(cmds=f"""get.pnl.data.from.monitor(date={date.strftime("%Y%m%d")}, symbols=c({",".join('"' + symbol + '"' for symbol in symbols)}), remote.server="{monitor_server}", nodes={nodes_str})""")

    # columns are: time tot.pnl
    vals = defaultdict(list)
    for row in reader:
        for col_name, col_value in row.items():
            vals[col_name].append(col_value)
    
    plot = {
        "type": "plot",
        "title": symbols[0] + " PnL",
        "xlabel": "Time",
        "xtype": "ms",
        "ylabel": "Total PnL",
        "date": date.isoformat(),
        "plots": [{
            "x": vals['time'],
            "y": vals['tot.pnl'],
            "name": "Total Pnl",
        }]
    }
    
    result = [plot]
    print (json.dumps(result))
    
    return 0

    
# GET.EXECUTIONS
@cmdline(
    ANoneOr(AMonitorServer("Server to reads data from")),
    ADate("Date to read"),
    ANoneOr(AList(AProdServer("Servers to return. None for all"))),
    ANoneOr(AList(AExchange("Exchanges to return. None for all"))),
    ANoneOr(ANodeSet(None, "Nodes to return. None for all")),
    ANoneOr(AList(AInt("Ports to return. None for all"))),
    ANoneOr(AList(AString("Symbols to return. None for all"))),
    ANoneOr(AString("File to output to")),
    ABool("Print result to stdout"),
    ABool("Print in JSON format (or CSV otherwise)"),
)
def get_executions(monitor_server=None, date=datetime.datetime.today().date(), servers=None, exchanges=None, nodes=None,
                   ports=None, symbols=None, filename=None, print_result=True, json_format=True):
    "Read executions log"
    if monitor_server is None:
        monitor_server = self_monitor_server()
    if monitor_server is None and servers is not None and len(servers) > 0:
        region = servers[0].region
        monitor_server = get_region_shob_monitor_server(region)
    if monitor_server is None and nodes is not None and len(nodes) > 0:
        region = ANodeAttr().parse(list(nodes)[0]).server.region
        monitor_server = get_region_shob_monitor_server(region)
    if monitor_server is None:
        raise Exception("Could not deduce monitor server based on servers/nodes. Try to pass monitor_server explicitly")

    date = int(format_date(date))
    dir = "/data/userdata/permanent/monitor/prod"
    command = f"egrep -H -v -i \"(^execution|^token|^HEARTBEAT)\" {dir}/{date}/*/executions.stat | sed -e \"s/^.*prod\\/[^\\/]*\\/\\([^\\/]*\\)[^:]*:/\\1 /\""
    run_servers_command(servers=[monitor_server], command=command)

    task = list(executors[0].tasks.values())[0]
    if task.status != TaskStatus.Success:
        raise Exception(f"Failed running task. Task status: {task.status}")

    lines = task.stdout.split("\n")
    if len(lines) <= 1:
        raise Exception(f"No data found, probably no executions available.")

    nfields = min((len(line.split(" ")) for line in lines[:-1]), default=0)
    fields = ["omserver", "token", "time", "exchange", "symbol", "orderid", "shares", "price", "currency",
              "matchid", "liquidity", "ext_liquidity", "port_idx", "side", "add_flags", "port_idx_k", "client",
              "lcode", "account_name", "gid_trade_str", "node", "trader_family", "subunit", "exchange_time",
              "romg_id", "price.usd", "fee.local", "fee.usd"][:nfields]
    # if there aren't enough fields so node isn't present - we have bad output we just want to return runcode
    if "node" not in fields:
        raise Exception(f"Unexpected error: didn't find the key 'node' in results. First few lines: {lines[0:5]}")
    
    if servers:
        servers = {server.server_id for server in servers}
    if exchanges:
        exchanges = {exchange.istra_mark for exchange in exchanges}
    predicate = lambda row: (
        (not servers or node_to_server(row["node"]) in servers) and
        (not nodes or int(row["node"]) in nodes) and
        (not symbols or row["symbol"] in symbols) and
        (not exchanges or row["exchange"] in exchanges) and
        (not ports or int(row["port_idx"]) in ports)
    )
    reader = csv.DictReader(lines, delimiter=" ", fieldnames=fields)
    dump_csv_reader(reader=reader, json_format=json_format, print_result=print_result, filename=filename, predicate=predicate)
    return 0


def to_human_number(num: float) -> str:
    try:
        num = float(num)
    except:
        return "-"
    
    absnum = abs(num)
    if absnum <= 1e-3:
        return "0"
    if absnum < 1:
        return ("%.3f" % num).strip('0')
    if absnum < 1000:
        return "%d" % num
    
    data = [('', 0), ('K', 3), ('M', 6), ('B', 9)]
    keys = [v[1] for v in data]
    val = data[bisect.bisect_right(keys, math.log10(absnum)) - 1]
    normed_num = num / (10 ** val[1])

    if abs(normed_num) < 10:
        num_of_digits = 2
    elif abs(normed_num) < 100:
        num_of_digits = 1
    else:
        num_of_digits = 0
    fmt = "%.{0}f%s".format(num_of_digits)

    return fmt % (normed_num, val[0])

# GET.POSITION
@cmdline(
    ARegion("Region"),
    ADate("Date to read"),
    ABool("With leveraged net position"),
    ANoneOr(AString("File to output to")),
    ABool("Print result to stdout"),
    ABool("Print in JSON format (or CSV otherwise)"),
)
def get_position(region, date=datetime.datetime.today().date(), with_leverage=True, filename=None, print_result=True, json_format=True):
    with create_R_server():
        reader = R_read_csv(cmds=f"""dump.get.position(date={date.strftime("%Y%m%d")}, region="{region.name}", with.netpos.leveraged={'TRUE' if with_leverage else 'FALSE'})""")

    def extra_data_fun(rows):
        netpos_dict = Counter()
        abspos_dict = Counter()
        netpos_leverage_dict = Counter()
        netpos_total = 0
        abspos_total = 0
        netpos_leverage_total = 0
        for row in rows:
            val = float(row['value.usd'])
            netpos_dict[row['account']] += val
            abspos_dict[row['account']] += abs(val)
            netpos_total += val
            abspos_total += abs(val)
            if with_leverage:
                leveraged_val = float(row['netpos.leveraged.val.usd'])
                netpos_leverage_dict[row['account']] += leveraged_val
                netpos_leverage_total += leveraged_val

        result = "\n".join([f"{name} NetPos: {to_human_number(netpos_dict[name])}, AbsPos: {to_human_number(abspos_dict[name])}" + (f", NetPosLeveraged: {to_human_number(netpos_leverage_dict[name])}" if with_leverage else "") for name in netpos_dict.keys()] +
                           [f"TOTAL NetPos: {to_human_number(netpos_total)} AbsPos: {to_human_number(abspos_total)}" + (f", NetPosLeveraged: {to_human_number(netpos_leverage_total)}" if with_leverage else "")])
        return result

    dump_csv_reader(reader=reader, json_format=json_format, print_result=print_result, filename=filename, extra_data_fun=extra_data_fun)
    return 0

def dump_csv_reader(reader, json_format, print_result, filename, predicate=lambda row: True, transform=lambda row: row, extra_data_fun = lambda rows: None):
    rv = [transform(dict(row)) for row in reader if predicate(row)]  # dict because Python 3.6 returns OrderedDict (while Python 3.8 returns dict - damn you Python and backward compatabilty!)
    ios = io.StringIO()
    if json_format:
        result = [dict(type="table", data=rv)]
        extra_data = extra_data_fun(rv)
        if extra_data is not None:
            result.append(dict(type="text", data=extra_data))
        json.dump(result, ios)
    else:
        writer = csv.DictWriter(ios, reader.fieldnames)
        writer.writeheader()
        for row in rv:
            writer.writerow(row)

    if print_result:
        sys.stdout.write(ios.getvalue())

    if filename:
        with open(filename, "wt") as f:
            f.write(ios.getvalue())

    return rv


@cmdline(
    AOneOf(AInt("iteration"), [1,2]),
    ASchedName("sched_name"),
    AOneOf(AString("broker"), ["BAML", "CIBC"]),
    ADate("date"),
    ABool("run_exclude_flow"),
    ABool("send_locates_email")
)
def locates_flow(iteration, sched_name, broker, date=datetime.datetime.today().date(), run_exclude_flow=True, send_locates_email=True):
    setup_cmds = [f"""options(from.interventor=TRUE)""",
                  f"""SET.MONITORED.REGION("{sched_name.region.name}")"""]
    if interventor_utils.configuration.simulation:
        setup_cmds.append("""options(OVERRIDE.LOCATES.BASE.PATH=file.path("/tmp/locates_dbg",repo.path()))""")

    with create_R_server(hide_stdout=False):
        run_R_cmds(cmds=setup_cmds + [
            f"""LOCATES.FLOW(iteration={iteration}, date={date.strftime("%Y%m%d")}, """ +
            f"""sched.name="{sched_name.name}", variant="{broker}", run.exclude.flow={boolean_to_r_boolean(run_exclude_flow)}, """ +
            f"""send.locates.email={boolean_to_r_boolean(send_locates_email)})"""])

    return 0

# SET.RLE.SERVER
@cmdline(
    AList(ARLEServer("RLE servers to interact with")),
    AOneOf(AString("RLE Action"), ["enable", "disable", "unblock", "increase_pnl_limit", "restore_pnl_limit", "increase_position_limit", "restore_position_limit"]),
    AString("Account to modify. Use @GLOBAL for all account."),
    AOneOf(AString("Send Via"), ["ssh", "chat_server"]),
    ANoneOr(AList(AProdServer("Servers to use for chat_server capability"))),
)
def set_rle_server(servers, action="enable", account="@GLOBAL", send_via="ssh", chat_server_servers=None):
    "Enable/Disable enforcer server"
    assert servers, "No servers provided"
    if send_via == "ssh":
        assert not chat_server_servers, "chat_server_servers provided by sending via ssh. This makes no sense"
        accounts_opt = "" if account == "@GLOBAL" else f"--accounts {account}"
        return run_per_server_commands([(server.hostname, [f"{server.repo_dirpath}/distribution/enforcer_server_controller.sh {accounts_opt} {action}"]) for server in servers])
    else:
        # Super hacky piece of junk.. the per monitor server commands are actually SSH commands 
        # to production server to use their chat server to talk with the monitor servers.. we only want
        # one to succeed so we also reverse the result so if one is successful we make it errornous so 
        # executor will stop running these commands. we then reverse the result in the output so success
        # is actually error and error is actually success.
        # very, very ugly.. but works.. but ugly.. yes.. very!
        if chat_server_servers is None:
            chat_server_servers = servers[0].region.production_servers

        assert chat_server_servers, "chat_server option selected but no servers to send this through"
        action = action.upper()
        accounts_opt = "" if account == "@GLOBAL" else account
        all_commands = dict()
        for server in servers:
            command = f"{PROD_SCRIPTS_PATH}/enforcer_daemon_controller.sh intervention {server.hostname} {action} {accounts_opt}"
            wrapped_commands = get_wrapped_commands([(s.server_id, [command]) for s in chat_server_servers], None)
            all_commands[server.hostname] = [command for server, commands in wrapped_commands for command in commands]
        return run_full_commands(list(all_commands.items()), reverse_result=True)


@cmdline(SERVERS_ATYPE,
         AString("Intervention log"),
         ADate("date"))
def add_intervention(servers, intervention_text, date=datetime.datetime.today().date()):
    "Send intervention log to one of the servers."
    filename = os.path.join("/home/istra/runs/prod", format_date(date), "my_serverid", "intervention.txt")
    command = ProcessUtils.list2cmd(["echo", intervention_text]) + " >> " + filename
    wrapped_commands = get_wrapped_commands([(s.server_id, [command]) for s in servers], None)

    all_commands = [command for server, commands in wrapped_commands for command in commands]
    return run_commands_serially_until_success(all_commands)

# SET.RLE.DAEMON
@cmdline(
    SERVERS_ONLY_ATYPE,
    AOneOf(AString("RLE Action"), ['status', 'unblock', 'heartbeat']),
)
def set_rle_daemon(servers, action):
    "Control enforcer daemon"
    return run_servers_command(command=f"{PROD_SCRIPTS_PATH}/enforcer_daemon_controller.sh {action}", servers=servers)

@cmdline(
    ARLEServer("RLE servers to interact with"),
    AOneOf(AString("BAML_200Bytes Action"), ['create_file', 'create_and_send_to_ftp']),
    ADate("date"),
    AString("filename"),
    AString("GPG Filename"),
    AString("Worker Name"),
    AString("FTP Filename"),
)
def upload_baml_200bytes(shob_server, action, date=datetime.datetime.today(), filename=f'''/tmp/{datetime.datetime.today().strftime('%F.%H.%M.%S')}.200.bytes''',
                         gpg_filename=None, worker_name='WSB275XA.INBNDTRD', ftp_filename=None):
    if gpg_filename is None:
        gpg_filename = f'{filename}.gpg'
    if ftp_filename is None:
        ftp_filename = os.path.join("incoming/mlpcc/", worker_name + date.strftime('%F.%H.%M.%S') + ".manual.200.bytes")
    base_path = shob_server.repo_dirpath

    generate_file_cmd = ProcessUtils.list2cmd([os.path.join(base_path, "python/TwoHundredBytes/writer.py"), "process_folder", "date=" +
                                               date.strftime("%Y%m%d"), "header_date=" + datetime.datetime.today().strftime('%Y%m%d'), "input_folder=" +
                                               os.path.join('/data/userdata/permanent/monitor/prod/', date.strftime('%Y%m%d')), "output_file=" + filename])
    gpg_file_cmd = ProcessUtils.list2cmd(["gpg", "--batch", "--yes", "-r", "clear@ml.com", "--always-trust", "-o", gpg_filename, "-a", "-e", filename])

    cmd = f"{generate_file_cmd} && {gpg_file_cmd}"
    if action == 'create_and_send_to_ftp':
        upload_file_cmd = ProcessUtils.list2cmd([os.path.join(base_path, "python/TwoHundredBytes/sender.py"), "send", "filename=" + gpg_filename, "server=ftp.b2b.ml.com",  "username=istrallc", "password=istr8932",  "dest_filename=" + ftp_filename])
        cmd += f"&& {upload_file_cmd}"

    return run_servers_command(servers=[shob_server], command=cmd)

@cmdline(
    AMonitorServer("200 bytes worker"),
    ADate("date"),
)
def get_baml_200bytes_status(shob_server, date):
    monitor_server = get_monitor_server()
    data = subprocess.run(ProcessUtils.list2cmd(["ssh", monitor_server.hostname, "egrep -i \'[0-9:.]* (SENT_FILE|MAIL) \'", f"/data/userdata/permanent/monitor/prod/{date.strftime('''%Y%m%d''')}/{shob_server.hostname}/200_bytes_worker.txt"]), shell=True, stdout=PIPE, encoding="utf-8", check=False).stdout
    dataIO = io.StringIO(data)
    reader = csv.reader(dataIO, delimiter=' ')
    uploaded_files = dict()
    confirmed_files = []
    for record in reader:
        if record[1] == "SENT_FILE":
            record[-1:] = "", "FALSE"
            uploaded_files[os.path.basename(record[2])] = record
        elif record[1] == "MAIL":
            confirmed_files.append(record)
    for record in confirmed_files:
        if record[7] in uploaded_files:
            uploaded_files[record[7]][-2:] = record[6], "TRUE"

    files = []
    headers = ["timestamp", "type", "filename", "number_of_executions", "rows_count", "file_size", "gpg_file_size", "original_filename", "result_code", "confirmed"]
    for record in uploaded_files.values():
        files.append(dict(zip(headers, record)))
    result = [dict(type="table", data=files)]
    print(json.dumps(result))
    return 0

@cmdline(
    ARLEServer("RLE servers to interact with"),
    AOneOf(AString("BAML_200Bytes Action"), ['start', 'stop', 'restart', 'restart_and_resend_files',
                                             'create_file', 'create_and_send_to_ftp']),
    ADate("date")
)
def set_baml_200bytes(shob_server, action, date):
    if action in ('create_file', 'create_and_send_to_ftp'):
        return upload_baml_200bytes(shob_server, action, date)

    base_path = shob_server.repo_dirpath
    date = date.strftime("%Y%m%d")
    if action in ('stop', 'restart', 'restart_and_resend_files'):
        cmd = ProcessUtils.list2cmd(["pkill", "-f", "[T]woHundredBytes/worker.py"])
    if action == 'restart_and_resend_files':
        cmd = ProcessUtils.list2cmd([base_path, "pkill", "-f", "[T]woHundredBytes/worker.py", "wipe_cache", "date=" + date])
    if action in ('start', 'restart', 'restart_and_resend_files'):
        cmd = ProcessUtils.list2cmd([os.path.join(base_path, "python/ShellUtils/repeater.py"), "run", "shell=True", "command=" +
                                     ProcessUtils.list2cmd([os.path.join(base_path, "python/TwoHundredBytes/worker.py"), "run", "date=" + date]),
                                    "cmd_id=two_hundreds_bytes"]) + "> /dev/null 2>&1"

    return run_servers_command(servers=[shob_server], command=cmd, ssh_flags="-f" if action in ('start', 'restart', 'restart_and_resend_files') else "")

# SET.ACRS
@cmdline(
    SERVERS_ONLY_ATYPE,
    AOneOf(AString("ACRS state to set"), ACRS_STATES)
)
def set_acrs(servers, state):
    "Enable/Disable ACRS checks"
    return run_sched_day_run(servers=servers, func=sched_day_run.send_mh_rcmd, kwargs=dict(rcmd=rc_set_acrs_state(state), b_rcmd_output=True))

@cmdline(ANodeSet(None, "List of nodes"),
         AStringList("List of symbols"),
         AString("TRE group name"),
         ABool("persistent"))
def prevent_overnight_position(nodes, symbols, tre_group_name="", persistent=True):
    rcmd = rc_prevent_overnight_position(symbols, tre_group_name)
    return run_sched_day_run(nodes=nodes,
                             func=sched_day_run.send_rcmd,
                             kwargs=dict(rcmd=rcmd,
                                         persistent=persistent,
                                         b_rcmd_output=True))

@cmdline(
    ANodeSet(None, "List of nodes to apply this to and restart"),
    AOr("List of strategies to symbols to run", 
        AJsonObject("JSON that contains [(Strategy, [Symbols])]"), 
        AList(ATuple("Strategy to Symbols pair", AString("Strategy"), AList(AString("Symbol"))))),
    ADate("The target running date"),
)
def exclusion_symbols_json(nodes, per_strat_symbols, date=datetime.datetime.today().date()):
    "exclusion symbols"
    date = int(format_date(date))
    json_dict = dict(date=date, action="include", reason="risk", restart_nodes=True, nodes_to_restart=str(list(nodes)))
    json_dict["details"] = dict(per_strat_symbols)
    dst_filename = "/home/istra/runs/prod/{date}/my_serverid/symbols_exclusion_{time}#{rand}".format(
        date=date, time=currtime_formatted(), rand=random.randint(1, 1e6))

    with tempfile.NamedTemporaryFile("wt", delete=False) as json_file:
        json.dump(json_dict, json_file)
        json_file.flush()
        logger.info("Generated the following JSON: {}".format(json.dumps(json_dict)))
        return run_sched_day_run(nodes=nodes, func=sched_day_run.exclusion_symbols_json, kwargs=dict(json=dst_filename),
                                 required_files=[(json_file.name, dst_filename)], override_func_nodes=False)


@cmdline(
    ANodeSet(node_roles.TRADING, "List of trading nodes to send to."),
    AOneOf(AString("State - enable or disable"), ["enable", "disable"]),
    AOneOf(AString("Whether to act on specific TRE groups or everything"), TRE_GROUP_MODES),
    AList(AString("Groups to act on")),
    AFromFun(sched_day_run.send_rcmd, "persistent")
)
def set_tre(nodes, state, mode="GROUPS", groups=None, persistent=True):
    "Enable/Disable TRE (tre will not suspend) on a list of groups or all groups"
    f = tre_enable if state == "enable" else tre_disable

    return f(nodes=nodes, mode=mode, groups=groups, persistent=persistent)

# TRE.ENABLE
@cmdline(
    ANodeSet(node_roles.TRADING, "List of trading nodes to send to."),
    AOneOf(AString("Whether to enable specific TRE groups or everything"), TRE_GROUP_MODES),
    AList(AString("Groups to enable")),
    AFromFun(sched_day_run.send_rcmd, "persistent")
)
def tre_enable(nodes, mode="GROUPS", groups=None, persistent=True):
    "Enable TRE (tre will not suspend) on a list of groups or all groups"
    return run_sched_day_run(nodes=nodes, func=sched_day_run.send_rcmd, 
                             kwargs=dict(rcmd=rc_enable_tre_groups(mode, groups), 
                                         persistent=persistent,
                                         b_rcmd_output=True))

# TRE.DISABLE
@cmdline(
    ANodeSet(node_roles.TRADING, "List of trading nodes to send to."),
    AOneOf(AString("Whether to disable specific TRE groups or everything"), TRE_GROUP_MODES),
    AList(AString("Groups to disable")),
    AFromFun(sched_day_run.send_rcmd, "persistent")
)
def tre_disable(nodes, mode="GROUPS", groups=None, persistent=True):
    "Disable TRE (tre will not suspend) on a list of groups or all groups"
    return run_sched_day_run(nodes=nodes, func=sched_day_run.send_rcmd, 
                             kwargs=dict(rcmd=rc_disable_tre_groups(mode, groups), 
                                         persistent=persistent,
                                         b_rcmd_output=True))


# B3.GAPS.RESTART
@cmdline()
def b3_gaps_restart(**kwargs):
    trading_servers = REGIONS["SouthAmerica"].production_servers
    
    status = stop_trading_nodes(servers=trading_servers, nodes=None, **kwargs)

    if status != 0:
        return status

    status = stop_feedhandler_nodes(servers=trading_servers, nodes=None, **kwargs)

    if status != 0:
        return status

    servers = REGIONS["SouthAmerica"].recording_servers

    status = kill_book_supplier_nodes(servers=servers, **kwargs)

    if status != 0:
        return status

    status = start_booksupplier_nodes(servers=servers, **kwargs)

    if status != 0:
        return status

    feedhandler_nodes = [node.node_id for server in trading_servers for node in server.nodes.values() if node.is_active and node.role == "FEED_HANDLER"]
    
    status = start_feedhandler_nodes(servers=None, nodes=feedhandler_nodes, **kwargs)

    if status != 0:
        return status

    trading_nodes = [node.node_id for server in trading_servers for node in server.nodes.values() if node.is_active and node.role == "TRADING"]

    return start_trading_nodes(servers=None, nodes=trading_nodes, **kwargs)

# KILL.BOOK.SUPPLIER.NODES
@cmdline_extend(
    AList(ARecordingServer("recording server to send command to")),
    from_func=sched_day_run.stop_booksupplier_nodes,
    new_defaults=dict(date=None, force_stop=True),
    fixed_defaults=dict(interactive=False)
)
def kill_book_supplier_nodes(servers, **kwargs):
    return run_sched_day_run(servers=servers, func=sched_day_run.stop_booksupplier_nodes, kwargs=kwargs)


# KILL.PERIPHERALS
@cmdline_extend(
    SERVERS_ONLY_ATYPE,
    from_func=sched_day_run.kill_all_peripherals,
    new_defaults=dict(date=None),
    fixed_defaults=dict(interactive=False)    
)
def kill_peripherals(servers, **kwargs):
    return run_sched_day_run(servers=servers, func=sched_day_run.kill_all_peripherals, kwargs=kwargs)


def get_omserver_summary_csv_reader(servers, nodes=None, mode="brief"):
    flag = OMSERVER_FLAGS[mode]
    run_servers_command(command=f"{PROD_SCRIPTS_PATH}/omserver.py query --{flag}", servers=servers)
    header = None
    all_lines = []
    for task in executors[0].tasks.values():
        if task.status == TaskStatus.Success:
            lines = task.stdout.split("\n")
            if len(lines) < 5:
                continue
            header = lines[2]
            all_lines += lines[3:-2]

    if header:
        for row in csv.DictReader([header]+all_lines):
            if nodes and "Node" in row.keys() and int(row["Node"]) not in nodes:
                continue
            if mode == "inflight" and int(row["InflightAdd"]) != 1:
                continue
            yield row

# GET.OMSERVER.SUMMARY
@cmdline(
    SERVERS_ONLY_ATYPE,
    ANoneOr(ANodeSet(None, "Nodes to filter")),
    AOneOf(AString("Mode"), list(OMSERVER_FLAGS.keys())),
    ANoneOr(AString("File to output csv to")),
    ABool("Print result to stdout?"),
    ABool("Format as JSON instead of CSV?"),
)
def get_omserver_summary(servers, nodes=None, mode="brief", filename=None, print_result=True, json_format=True):
    "Summary based on omserver info"
    reader = get_omserver_summary_csv_reader(servers=servers, nodes=nodes, mode=mode)

    def transform(row):
        for f in ('time', "Timestamp", "exchange_timestamp"):
            if f in row:
                row[f] = TimeUtils.us2string(int(row[f]))
        return row

    dump_csv_reader(reader=reader, json_format=json_format, print_result=print_result, filename=filename, transform=transform)
    return 0


# TBDs
# -----------
# LOWER PRIORITY TODOs
# -----------------------
# TODO: BAML.LOCATES.PRE.FLOW.EXCLUDE
# TODO: BAML.LOCATE
# TODO: BAML.LOCATE.CHECK.AND.INCLUDE
# TODO: RESTART.AND.RESET.OMSERVER
# TODO: AUTO.SHORT.MARKING.RECONFIGURE
# TODO: SET.BAML.200BYTES

# JP.CREATE.MANUAL.LIQUIDATION.FILE
@cmdline(
    ABool("Is price in USD"),
    AString("Price aggressiveness"),
    ADate("Date to read"),
    ANoneOr(AString("File to output csv to")),
    ABool("Print result to stdout?"),
    ABool("Format as JSON instead of CSV?"),
)
def get_jp_manual_liquidateion_file(price_in_usd,
                                    price_aggressiveness,
                                    date=datetime.datetime.today().date(),
                                    filename=None,
                                    print_result=True,
                                    json_format=True):
    with create_R_server():
        reader = R_read_csv(cmds=f"""JP.CREATE.MANUAL.LIQUIDATION.FILE(date={date.strftime("%Y%m%d")}, price.in.USD={"TRUE" if price_in_usd else "FALSE"}, price.aggressiveness={price_aggressiveness},save.res.path=NA)""")
    dump_csv_reader(reader=reader, json_format=json_format, print_result=print_result, filename=filename)
    return 0

# AUTO.SHORT.MARKING.RECONFIGURE
@cmdline(
    AOneOf(AString("Action"), ["allow_some", "disallow_some", "set_disallowed"]),
    ANodeSet(node_roles.TRADING, "List of trading nodes to send to."),
    AStringList("List of symbols. Use @ALL for all.")
)
def auto_short_marking_reconfigure(action,
                                   nodes,
                                   symbols=["@ALL"]):
    "Allow/disallow short marking on symbols"
    accurate_symbols = set()
    inaccurate_symbols = set()
    symbols = set(symbols)

    if action == "allow_some":
        accurate_symbols = accurate_symbols.union(symbols)
    elif action == "disallow_some":
        inaccurate_symbols = inaccurate_symbols.union(symbols)
    elif action == "set_disallowed":
        accurate_symbols = set() if (symbols == {"@ALL"}) else {"@ALL"}
        inaccurate_symbols = inaccurate_symbols.union(symbols)
    else:
        raise Exception(f"Unexpected action: {action}")

    return run_sched_day_run(nodes=nodes,
            func=sched_day_run.send_mh_rcmd,
            kwargs=dict(
                rcmd=rc_set_accurate_short_marking(
                    accurate_symbols=list(accurate_symbols),
                    inaccurate_symbols=list(inaccurate_symbols)),
                b_rcmd_output=True))

@cmdline_extend(
    SERVERS_ATYPE,
    from_func=sched_day_run.check_finished,
    new_defaults=dict(nodes=None, date=None)
)
def check_finished(servers=None, **kwargs):
    return run_sched_day_run(servers=servers, nodes=kwargs["nodes"], func=sched_day_run.check_finished, kwargs=kwargs)


@cmdline(
    SERVERS_ONLY_ATYPE
)
def markethandler_status(servers):
    "See market handlers status"
    return run_sched_day_run(servers=servers, func=sched_day_run.send_mh_rcmd, kwargs=dict(rcmd=rc_process_manager_status(), b_rcmd_output=True))

# UPDATE.INI.CONFIG
@cmdline_extend(
    from_func=sched_day_run.add_consts_override,
    new_defaults=dict(nodes=None, date=None),
)
def update_ini_config(**kwargs):
    return run_sched_day_run(nodes=kwargs["nodes"], func=sched_day_run.add_consts_override, kwargs=kwargs)

# RESTART.AND.RESET.OMSERVER
@cmdline(
    SERVERS_ONLY_ATYPE,
)
def restart_and_reset_omserver(servers):
    "Restart and reset omserver on production servers"
    command = f"{PROD_SCRIPTS_PATH}/taskmng.py kill --tasks omserver && sleep 5 && {PROD_SCRIPTS_PATH}/omserver.py reset --erase_logs && {PROD_SCRIPTS_PATH}/taskmng.py start --tasks omserver"
    return run_servers_command(command=command, servers=servers)


# TODO: setup interactive if __name__ == "__main__" (require region, etc)
#       otherwise setup for "web" runs (no need for validations, etc)
if __name__ == '__main__':
    rv = 3
    try:
        # TODO: this is just a temporary "hack" so I can run stuff on real servers while testing - this should be done better!
        interventor_utils.configuration.runner = interventor_utils.RunnerType.USER
        if "--real" in sys.argv:
            interventor_utils.configuration.simulation = False
            sys.argv.remove("--real")
        if "--json" in sys.argv:
            interventor_utils.configuration.runner = interventor_utils.RunnerType.JSON
            sys.argv.remove("--json")
        if "--outfile" in sys.argv:
            interventor_utils.configuration.runner = interventor_utils.RunnerType.OUTPUT_FILE
            sys.argv.remove("--outfile")
        logger.info(f"Running interventor. Simulation: {interventor_utils.configuration.simulation}; args: {sys.argv}")
        rv = shellathon.run()
    except Exception as ex:
        logger.exception(ex)
        logger.error("Exiting due to error: {0}".format(ex))
        rv = 1

    logger.info(f"Done. Return value: {rv}")
    sys.exit(rv)
