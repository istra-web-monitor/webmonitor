#!/bin/bash

cd "$(dirname "$0")" || exit

set -euo pipefail

# Run Celery on bg
DATE=$(date '+%Y%m%d')
LOG_FOLDER=/data/logs/web_intervention/"$DATE"
mkdir -p "$LOG_FOLDER"
/usr/istra/apps/pypy3.6-v7.3.1-linux64/bin/celery worker -A web_intervention_server.celery --loglevel=info --concurrency=10 --logfile "$LOG_FOLDER/celery_$(date '+%Y%m%d_%H_%M_%S').log" & pid1=$!

# Run Flower (Celery monitor), listens to port 5555
(/usr/istra/apps/pypy3.6-v7.3.1-linux64/bin/celery -A web_intervention_server.celery flower || echo "Failed running Flower Celery monitor (not critical)") & pid2=$!

trap on_finish EXIT
on_finish() { kill -SIGINT $pid1 $pid2 ; pkill -f 'web_intervention_server:app'; wait; }

# For production - run FastAPI under uvicorn
/usr/istra/apps/pypy3.6-v7.3.1-linux64/bin/uvicorn web_intervention_server:app --port 5000 --loop asyncio --host 0.0.0.0

