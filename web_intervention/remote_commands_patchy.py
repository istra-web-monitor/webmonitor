from python.XParamUtils import *
from OrderEntryTool.OrderEntryXParamUtils import xparam

# This is a horrible idea but is supposed to be "temporary" until this becomes popular enough
# or the need forces us to generate python versions along side R ones from codegen.
#
# WARNING: None of this is auto-generated, changing the original .cg will break it.
#          this is exactly why it's a horrible idea. but don't shoot me - I'm only the messenger!

ACRS_STATES = ["Enabled", "RejectAllOrders", "AllowAllOrders"]
def rc_set_acrs_state(state):
    assert state in ACRS_STATES, "invalid state {}".format(state)
    return f"RC_SetAccountRiskStatsFactory(RC_SetAccountRiskStatsParams(\"@ALL\",ObserverID(ACCOUNT_RISK_STATS_ID),ACRS_State(ACRS_State_{state})))"

def rc_prevent_overnight_position(symbols, tre_group_name):
    return xparam.customclass("RC_MarketeerEnableOvernightCmdFactory",
                              xparam.customclass("RC_MarketeerEnableOvernightCmdParams",
                                                 xparam.bool(True),  # force
                                                 xparam.bool(False), # force.allow
                                                 ## TODO: Should we encode symbols?
                                                 xparam.svec("string", xparam.string, symbols),
                                                 xparam.string(tre_group_name)))

def rc_process_manager_status():
    return "RC_ProcessManagerStatusFactory()"

TRE_GROUP_MODES = ["GROUPS", "ALL_GROUPS"]
def rc_enable_tre_groups(mode, groups):
    assert mode in TRE_GROUP_MODES, "invalid group mode {}".format(mode)
    if mode == "GROUPS":
        assert groups, "No groups given but 'GROUPS' mode selected"
        return "RC_enableTREGroupSuspensionsFactory(RC_enableTREGroupSuspensionsParams({}))".format(xparam.svec("string", xparam.string, groups))
    else:
        return "RC_enableAllTREGroupSuspensionsFactory()"

def rc_disable_tre_groups(mode, groups):
    assert mode in TRE_GROUP_MODES, "invalid group mode {}".format(mode)
    if mode == "GROUPS":
        assert groups, "No groups given but 'GROUPS' mode selected"
        return "RC_disableTREGroupSuspensionsFactory(RC_disableTREGroupSuspensionsParams({}))".format(xparam.svec("string", xparam.string, groups))
    else:
        return "RC_disableAllTREGroupSuspensionsFactory()"

def rc_set_strats_exclusion(symbols, reason, strategies, bExclude):
    return xparam.customclass(
        "RC_setStrategiesExclusionFactory",
        xparam.customclass(
            "RC_setStrategiesExclusionParams",
            xparam.svec("string", xparam.string, symbols),
            xparam.string(reason),
            xparam.svec("string", xparam.string, strategies),
            xparam.bool(bExclude)))

def rc_modify_romg_orders(orders, action):
    return xparam.customclass(
        "RC_modifyRemoteOMGOrdersFactory",
        xparam.customclass(
            "RC_modifyRemoteOMGOrdersParams",
            xparam.svec("string", xparam.string, orders),
            xparam.enum_class("ActionOnOrder", action)
        )
    )

def rc_manual_liquidate(symbols):
    return xparam.customclass(
        "RC_ManualLiquidateFactory",
        xparam.customclass(
            "RC_ManualLiquidateParams",
            xparam.svec("string", xparam.string, symbols)))

def rc_set_accurate_short_marking(accurate_symbols, inaccurate_symbols):
    return xparam.customclass("RC_setAccurateShortMarkingFactory",
                              xparam.customclass(
                                  "RC_setAccurateShortMarkingParams",
                                  xparam.svec("string", xparam.string, accurate_symbols),
                                  xparam.svec("string", xparam.string, inaccurate_symbols),
                              ))

def rc_set_symbols_exit_time(symbols, time):
    return xparam.customclass(
        "RC_setSymbolsExitTimeFactory",
        xparam.customclass(
            "RC_setSymbolsExitTimeParams",
            xparam.svec("string", xparam.string, symbols),
            xparam.u32(time)
        ))
