#!/usr/istra/bin/pypy3

import time, random, json, click, sys, datetime
import logging as logger
from colorama import Fore, Style
from collections import defaultdict
logger.basicConfig(format='[%(asctime)s] %(levelname)s (' + Fore.CYAN + '%(filename)s:%(lineno)d %(funcName)s' + Style.RESET_ALL + ') %(message)s', level=logger.DEBUG)

def _run(name, nodes=None, variant=None, restart=False, parallel=True, bypass_warnings=False, servers=None):
    if servers is None:
        assert nodes is not None
        servers_dict = defaultdict(list)
        for node in nodes:
            servers_dict[int(node) // 100 * 100].append(node)
    else:
        servers_dict = {s: [s] for s in servers}

    for server, server_nodes in servers_dict.items():
        print(json.dumps(dict(
            server=server,
            return_code=None,
            status="Running",
            stdout="",
            stderr=f"{server}: started command (pid: {random.randint(100, 5000)}): ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no -l istra prod{server} '/home/istra/scripts/sched_day_run.py {name} running_mode=\"prod\" date=20200309 nodes='[{', '.join(str(n) for n in server_nodes)}]'\n")), flush=True)
    
    for server in servers_dict.keys():
        print(json.dumps(dict(server=server, return_code=None, status="Running", stdout="", stderr="")), flush=True)

    time.sleep(10)

    for server, server_nodes in servers_dict.items():
        print(json.dumps(dict(
            server=server,
            return_code=0,
            status="Success",
            stdout="\n".join(f"{n} IS DONE" for n in server_nodes),
            stderr=f"[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.log\n[20200309 19:11:13] INFO  Running on prod machine prod{server}\n[20200309 19:11:13] INFO  Finished\n")), flush=True)
        time.sleep(10)


@click.group()
def cli():
    pass

@cli.command(name="start_services")
@click.option("--servers", multiple=True, type=int)
def clisk_start_services(servers):
    _run(name="start_services", servers=servers)

@cli.command(name="liquidate")
@click.option("--node-symbols")
def click_liquidate(node_symbols):
    node_symbols = json.loads(node_symbols)
    _run(name="liquidate", nodes=node_symbols.keys())

@cli.command(name="exclusion_orderentry_port")
@click.option("--servers", multiple=True, type=int)
@click.option("--ports", multiple=True, type=int)
@click.option("--action")
def click_exclusion_orderentry_port(servers, ports=[], action=""):
    _run(name="exclusion_orderentry_port", servers=servers)

@cli.command(name="exclusion_feed")
@click.option("--nodes", multiple=True, type=int)
def click_exclusion_feed(nodes):
    _run(name="exclusion_feed", nodes=nodes)

@cli.command(name="exclusion_orderentry_exchange")
@click.option("--servers", multiple=True, type=int)
@click.option("--mh_servers", multiple=True, type=int)
@click.option("--action")
@click.option("--exchanges", multiple=True)
@click.option("--restart_nodes", is_flag=True)
def click_exclusion_orderentry_exchange(servers, mh_servers, action, exchanges, restart_nodes=False):
    _run(name="exclusion_orderentry_exchange", servers=servers)

@cli.command(name="exclusion_orderentry_link")
@click.option("--action")
@click.option("--servers", multiple=True, type=int)
@click.option("--mh_servers", multiple=True, type=int)
def click_exclusion_orderentry_link(action, servers, mh_servers):
    _run(name="exclusion_orderentry_link", servers=servers)

@cli.command(name="modify_order_state_in_omserver")
@click.option("--action")
@click.option("--server-to-orderids")
def click_modify_order_state_in_omserver(action, server_to_orderids):
    server_to_orderids = json.loads(server_to_orderids)
    _run(name="modify_order_state_in_omserver", servers=[s[0] for s in server_to_orderids])

@cli.command(name="start_trading_nodes")
@click.option("--nodes", multiple=True, type=int)
@click.option("--variant")
@click.option("--restart", is_flag=True)
@click.option("--parallel/--no-parallel", default=True)
@click.option("--bypass-warnings", is_flag=True)
def click_start_trading_nodes(nodes, variant=None, restart=False, parallel=True, bypass_warnings=False):
    _run(name="start_trading_nodes", nodes=nodes, variant=variant, restart=restart, parallel=parallel, bypass_warnings=bypass_warnings)

@cli.command(name="start_nodes")
@click.option("--nodes", multiple=True, type=int)
@click.option("--variant")
@click.option("--restart", is_flag=True)
@click.option("--parallel/--no-parallel", default=True)
@click.option("--bypass-warnings", is_flag=True)
def click_start_nodes(nodes, variant=None, restart=False, parallel=True, bypass_warnings=False):
    _run(name="start_nodes", nodes=nodes, variant=variant, restart=restart, parallel=parallel, bypass_warnings=bypass_warnings)

@cli.command(name="kill_trading_nodes")
@click.option("--nodes", multiple=True, type=int)
@click.option("--strategy", default="all")
@click.option("--parallel/--no-parallel", default=True)
@click.option("--bypass-warnings", is_flag=True)
@click.option("--include-special-nodes", is_flag=True)
def click_kill_trading_nodes(nodes, strategy="all", parallel=True, bypass_warnings=False, include_special_nodes=False):
    _run(name="kill_trading_nodes", nodes=nodes, variant=variant, restart=restart, parallel=parallel, bypass_warnings=bypass_warnings)

@cli.command(name="kill_nodes")
@click.option("--nodes", multiple=True, type=int)
@click.option("--strategy", default="all")
@click.option("--parallel/--no-parallel", default=True)
@click.option("--bypass-warnings", is_flag=True)
@click.option("--include-special-nodes", is_flag=True)
def click_kill_nodes(nodes, strategy="all", parallel=True, bypass_warnings=False, include_special_nodes=False):
    _run(name="kill_nodes", nodes=nodes, variant=variant, restart=restart, parallel=parallel, bypass_warnings=bypass_warnings)

@cli.command(name="exclusion_symbols")
@click.option("--nodes", multiple=True, type=int)
@click.option("--action", type=click.Choice(["include", "exclude"]))
@click.option("--symbols", default=None)
@click.option("--strats", default=None)
@click.option("--reason", default=None)
@click.option("--parallel/--no-parallel", default=True)
@click.option("--bypass-warnings", is_flag=True)
@click.option("--restart_nodes", is_flag=True)
def click_exclusion_symbols(nodes, action, symbols=None, strats=None, reason=None, parallel=True, bypass_warnings=False, restart_nodes=False):
    exclusion_symbols(nodes=nodes, action=action, symbols=symbols, strats=strats, reason=reason, parallel=parallel, bypass_warnings=bypass_warnings, restart_nodes=restart_nodes)

def exclusion_symbols(nodes, action, symbols=None, strats=None, reason=None, parallel=True, bypass_warnings=False, restart_nodes=False, **kwargs):
    servers_dict = defaultdict(list)
    for node in nodes:
        servers_dict[int(node) // 100 * 100].append(node)
    for server, server_nodes in servers_dict.items():
        print(json.dumps(dict(
            server=server,
            return_code=None,
            status="Running",
            stdout="",
            stderr=f"{server}: started command (pid: {random.randint(100, 5000)}): ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no -l istra prod{server} '/home/istra/scripts/sched_day_run.py exclusion_symbols running_mode=\"prod\" date=20200309 nodes='[{', '.join(str(n) for n in server_nodes)}]'\n")), flush=True)
    time.sleep(0.1)

    for server in servers_dict.keys():
        print(json.dumps(dict(server=server, return_code=None, status="Running", stdout="", stderr="")))

    time.sleep(3)

    for server, server_nodes in servers_dict.items():
        print(json.dumps(dict(
            server=server,
            return_code=1,
            status="Error",
            stdout="\n".join(f"{n} Failed to connect" for n in server_nodes),
            stderr=f"[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.lo[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.logggggggggggggggggggggggggggggggggg[20200309 19:11:13] INFO  Log path: /home/istra/logs/20200309/sched_day_run__20200309_191113_359225.log\n[20200309 19:11:13] INFO  Running on prod machine prod{server}\n[20200309 19:11:13] INFO  Finished\n")), flush=True)

    raise Exception("Not all servers completed successfully")


@cli.command(name="get_omserver_summary")
@click.option("--servers", multiple=True, type=int)
@click.option("--mode")
def get_omserver_summary(servers, mode):
    print("Getting data", file=sys.stderr)
    time.sleep(1)
    if mode == "brief":
        print("""[{"type": "table", "data": [{"Node": "410215", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "533", "SharesTraded": "1913394", "KDollarsTraded": "3816.436", "LastExecTime": "16:10:40.364616"}, {"Node": "410203", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "572", "SharesTraded": "579458", "KDollarsTraded": "1328.487", "LastExecTime": "16:10:40.046440"}, {"Node": "410206", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "759", "SharesTraded": "1055178", "KDollarsTraded": "2644.967", "LastExecTime": "16:10:40.364616"}, {"Node": "410211", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "365", "SharesTraded": "482017", "KDollarsTraded": "1646.029", "LastExecTime": "15:59:54.315362"}, {"Node": "410209", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "763", "SharesTraded": "780283", "KDollarsTraded": "2117.838", "LastExecTime": "16:10:40.306927"}, {"Node": "410201", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "608", "SharesTraded": "822819", "KDollarsTraded": "1015.172", "LastExecTime": "16:10:40.306927"}, {"Node": "410213", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "623", "SharesTraded": "1010908", "KDollarsTraded": "1767.420", "LastExecTime": "16:10:40.071512"}, {"Node": "410214", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "647", "SharesTraded": "1332238", "KDollarsTraded": "2968.758", "LastExecTime": "16:10:40.271167"}, {"Node": "410202", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "910", "SharesTraded": "2126442", "KDollarsTraded": "3633.504", "LastExecTime": "16:10:40.327511"}, {"Node": "410204", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "853", "SharesTraded": "1003378", "KDollarsTraded": "2586.359", "LastExecTime": "16:10:40.349782"}, {"Node": "410207", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "661", "SharesTraded": "1584182", "KDollarsTraded": "1904.718", "LastExecTime": "16:10:40.364616"}, {"Node": "410210", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "576", "SharesTraded": "680630", "KDollarsTraded": "2096.636", "LastExecTime": "16:10:40.327511"}, {"Node": "410208", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "800", "SharesTraded": "1169793", "KDollarsTraded": "2886.371", "LastExecTime": "16:10:40.306927"}, {"Node": "410205", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "468", "SharesTraded": "1089793", "KDollarsTraded": "1721.286", "LastExecTime": "16:10:40.031843"}, {"Node": "410212", "AbsPosKDollar": "nan", "NetPosKDollar": "nan", "OpenOrders": "0", "InflightOrders": "0", "PnL": "nan", "NumTrades": "424", "SharesTraded": "717740", "KDollarsTraded": "1252.768", "LastExecTime": "16:10:40.096716"}]}]""")
    else:
        print("""[{"type": "table", "data": [{"Node": "410215", "Symbol": "au:A2M", "Shares": "-2479", "Price": "18.860"}, {"Node": "410215", "Symbol": "au:AAC", "Shares": "3498", "Price": "1.040"}, {"Node": "410215", "Symbol": "au:ABC", "Shares": "3039", "Price": "2.360"}, {"Node": "410215", "Symbol": "au:ABP", "Shares": "13108", "Price": "2.710"}, {"Node": "410215", "Symbol": "au:AGL", "Shares": "4341", "Price": "17.030"}, {"Node": "410215", "Symbol": "au:AHY", "Shares": "7133", "Price": "0.950"}, {"Node": "410215", "Symbol": "au:AIA", "Shares": "3028", "Price": "5.810"}, {"Node": "410215", "Symbol": "au:ALL", "Shares": "348", "Price": "28.810"}, {"Node": "410215", "Symbol": "au:ALQ", "Shares": "-356", "Price": "9.000"}, {"Node": "410215", "Symbol": "au:ALU", "Shares": "-355", "Price": "32.760"}, {"Node": "410215", "Symbol": "au:AMA", "Shares": "26292", "Price": "0.540"}, {"Node": "410215", "Symbol": "au:AMC", "Shares": "708", "Price": "15.580"}, {"Node": "410215", "Symbol": "au:AMI", "Shares": "100861", "Price": "0.560"}, {"Node": "410215", "Symbol": "au:AMP", "Shares": "17099", "Price": "1.380"}, {"Node": "410215", "Symbol": "au:ANN", "Shares": "-1083", "Price": "39.690"}, {"Node": "410215", "Symbol": "au:ANZ", "Shares": "-761", "Price": "18.490"}, {"Node": "410215", "Symbol": "au:APA", "Shares": "-4129", "Price": "11.490"}, {"Node": "410215", "Symbol": "au:API", "Shares": "5162", "Price": "1.120"}, {"Node": "410215", "Symbol": "au:APT", "Shares": "-51", "Price": "70.180"}, {"Node": "410215", "Symbol": "au:APX", "Shares": "737", "Price": "36.170"}, {"Node": "410215", "Symbol": "au:ARB", "Shares": "-301", "Price": "22.030"}, {"Node": "410215", "Symbol": "au:ARF", "Shares": "3198", "Price": "2.280"}, {"Node": "410215", "Symbol": "au:ASB", "Shares": "-3139", "Price": "3.270"}, {"Node": "410215", "Symbol": "au:AST", "Shares": "-64469", "Price": "1.830"}, {"Node": "410215", "Symbol": "au:ASX", "Shares": "81", "Price": "84.310"}, {"Node": "410215", "Symbol": "au:AUB", "Shares": "581", "Price": "12.800"}, {"Node": "410215", "Symbol": "au:AVN", "Shares": "2212", "Price": "2.020"}, {"Node": "410215", "Symbol": "au:AWC", "Shares": "29442", "Price": "1.655"}, {"Node": "410215", "Symbol": "au:AZJ", "Shares": "-6682", "Price": "4.580"}, {"Node": "410215", "Symbol": "au:BAP", "Shares": "-125", "Price": "6.220"}, {"Node": "410215", "Symbol": "au:BBN", "Shares": "2944", "Price": "3.620"}, {"Node": "410215", "Symbol": "au:BEN", "Shares": "19053", "Price": "7.030"}, {"Node": "410215", "Symbol": "au:BGA", "Shares": "5276", "Price": "4.660"}, {"Node": "410215", "Symbol": "au:BHP", "Shares": "-1764", "Price": "40.200"}, {"Node": "410215", "Symbol": "au:BIN", "Shares": "3063", "Price": "1.985"}, {"Node": "410215", "Symbol": "au:BKL", "Shares": "43", "Price": "73.680"}, {"Node": "410215", "Symbol": "au:BKW", "Shares": "406", "Price": "16.720"}, {"Node": "410215", "Symbol": "au:BLD", "Shares": "-6854", "Price": "3.780"}, {"Node": "410215", "Symbol": "au:BOQ", "Shares": "3062", "Price": "6.150"}, {"Node": "410215", "Symbol": "au:BPT", "Shares": "-16847", "Price": "1.435"}, {"Node": "410215", "Symbol": "au:BRG", "Shares": "660", "Price": "28.380"}, {"Node": "410215", "Symbol": "au:BSL", "Shares": "-5170", "Price": "12.350"}, {"Node": "410215", "Symbol": "au:BVS", "Shares": "-4514", "Price": "4.170"}, {"Node": "410215", "Symbol": "au:BWP", "Shares": "-4426", "Price": "3.970"}, {"Node": "410215", "Symbol": "au:BWX", "Shares": "4230", "Price": "4.210"}, {"Node": "410215", "Symbol": "au:BXB", "Shares": "-3933", "Price": "11.060"}]}]""")
    print("Done", file=sys.stderr)


@cli.command(name="get_executions")
@click.option("--servers", multiple=True, type=int)
@click.option("--exchanges", multiple=True)
@click.option("--nodes", multiple=True, type=int)
@click.option("--ports", multiple=True, type=int)
@click.option("--symbols", multiple=True)
@click.option("--date")
def get_executions(servers=[], exchanges=[], nodes=[],
                   ports=[], symbols=[], date=datetime.datetime.today().date()):
    print("Getting data", file=sys.stderr)
    time.sleep(1)
    print("""[{"type": "table", "data": [{"omserver": "410200", "token": "0", "time": "36009057378", "exchange": "ASX", "symbol": "au:ALD", "orderid": "126975738881", "shares": "22", "price": "29.66", "currency": "AUD", "matchid": "773094113319", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "B", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:ALD,410202,17,148,0,1,ASX,0,1", "node": "410202", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "21.4470620561991", "fee.local": "-0.05773406", "fee.usd": "-0.04174733538692927"}, {"omserver": "410200", "token": "0", "time": "36009060238", "exchange": "ASX", "symbol": "au:ALX", "orderid": "116506759681", "shares": "41", "price": "6.44", "currency": "AUD", "matchid": "803158884411", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "B", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:ALX,410209,17,148,0,1,ASX,0,1", "node": "410209", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "4.6567457734970406", "fee.local": "-0.032288620000000004", "fee.usd": "-0.023347809738672672"}, {"omserver": "410200", "token": "0", "time": "36009061139", "exchange": "ASX", "symbol": "au:AIA", "orderid": "90200087553", "shares": "247", "price": "5.66", "currency": "AUD", "matchid": "816043786258", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "B", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:AIA,410214,17,148,0,1,ASX,0,1", "node": "410214", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "4.092729981054852", "fee.local": "-0.10656431", "fee.usd": "-0.07705635096244229"}, {"omserver": "410200", "token": "0", "time": "36009072050", "exchange": "ASX", "symbol": "au:BIN", "orderid": "91810698241", "shares": "978", "price": "1.915", "currency": "AUD", "matchid": "863288426538", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "B", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:BIN,410210,17,148,0,1,ASX,0,1", "node": "410210", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "1.3847310801625514", "fee.local": "-0.137666985", "fee.usd": "-0.09954660722807925"}, {"omserver": "410200", "token": "0", "time": "36009077238", "exchange": "ASX", "symbol": "au:BXB", "orderid": "119191114241", "shares": "35", "price": "10.92", "currency": "AUD", "matchid": "910533066846", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "S", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:BXB,410209,17,148,0,1,ASX,0,1", "node": "410209", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "7.896221094190633", "fee.local": "-0.0400281", "fee.usd": "-0.02894420582237839"}, {"omserver": "410200", "token": "0", "time": "36009077259", "exchange": "ASX", "symbol": "au:BXB", "orderid": "119191114241", "shares": "37", "price": "10.92", "currency": "AUD", "matchid": "910533066847", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "S", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:BXB,410209,17,148,0,1,ASX,0,2", "node": "410209", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "7.896221094190633", "fee.local": "-0.04145862", "fee.usd": "-0.02997861078571737"}, {"omserver": "410200", "token": "0", "time": "36009077281", "exchange": "ASX", "symbol": "au:BXB", "orderid": "119191114241", "shares": "612", "price": "10.92", "currency": "AUD", "matchid": "910533066848", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "S", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:BXB,410209,17,148,0,1,ASX,0,3", "node": "410209", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "7.896221094190633", "fee.local": "-0.45273312", "fee.usd": "-0.3273700377456721"}, {"omserver": "410200", "token": "0", "time": "36009077302", "exchange": "ASX", "symbol": "au:BXB", "orderid": "119191114241", "shares": "653", "price": "10.92", "currency": "AUD", "matchid": "910533066849", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "S", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:BXB,410209,17,148,0,1,ASX,0,4", "node": "410209", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "7.896221094190633", "fee.local": "-0.48205878", "fee.usd": "-0.34857533949412106"}, {"omserver": "410200", "token": "0", "time": "36009077324", "exchange": "ASX", "symbol": "au:BXB", "orderid": "119191114241", "shares": "225", "price": "10.92", "currency": "AUD", "matchid": "910533066850", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "S", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:BXB,410209,17,148,0,1,ASX,0,5", "node": "410209", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "7.896221094190633", "fee.local": "-0.1759275", "fee.usd": "-0.1272126773395808"}, {"omserver": "410200", "token": "0", "time": "36009077343", "exchange": "ASX", "symbol": "au:BXB", "orderid": "119191114241", "shares": "372", "price": "10.92", "currency": "AUD", "matchid": "910533066851", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28103", "side": "S", "add_flags": "68736253952", "port_idx_k": "28103", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:BXB,410209,17,148,0,1,ASX,0,6", "node": "410209", "trader_family": "148", "subunit": "17", "exchange_time": "36009023275", "romg_id": "410281", "price.usd": "7.896221094190633", "fee.local": "-0.28107072", "fee.usd": "-0.20324144214499534"}, {"omserver": "410200", "token": "0", "time": "36009080163", "exchange": "ASX", "symbol": "au:ABP", "orderid": "91273822721", "shares": "184", "price": "2.66", "currency": "AUD", "matchid": "51539607564", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28102", "side": "B", "add_flags": "68736253952", "port_idx_k": "28102", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:ABP,410201,17,148,0,1,ASX,0,1", "node": "410201", "trader_family": "148", "subunit": "17", "exchange_time": "36009023286", "romg_id": "410281", "price.usd": "1.923438471661821", "fee.local": "-0.04705232000000001", "fee.usd": "-0.03402339942441464"}, {"omserver": "410200", "token": "0", "time": "36009080194", "exchange": "ASX", "symbol": "au:ABP", "orderid": "91273822721", "shares": "81", "price": "2.66", "currency": "AUD", "matchid": "51539607565", "liquidity": "a", "ext_liquidity": "20/0", "port_idx": "28102", "side": "B", "add_flags": "68736253952", "port_idx_k": "28102", "client": "BAML", "lcode": "a", "account_name": "ISSAUSWAP", "gid_trade_str": "au:ABP,410201,17,148,0,1,ASX,0,2", "node": "410201", "trader_family": "148", "subunit": "17", "exchange_time": "36009023286", "romg_id": "410281", "price.usd": "1.923438471661821", "fee.local": "-0.02910663", "fee.usd": "-0.02104692177534816"}]}]""")
    print("Done", file=sys.stderr)

@cli.command(name="exclusion_status")
@click.option("--servers", multiple=True, type=int)
def exclusion_status(servers=[]):
    _run(name="exclusion_status", servers=servers)

@cli.command(name="get_position")
@click.option("--region")
@click.option("--date", type=datetime.date)
def get_position(region, date=None):
    print("Getting data", file=sys.stderr)
    time.sleep(1)
    print("""[{"type": "table", "data": [{"symbol": "au:LNK", "account": "ISSAUSWAP", "shares": "47", "price.usd": "3.07", "value.usd": "144.29"}, {"symbol": "au:REG", "account": "ISSAUSWAP", "shares": "-275", "price.usd": "0.97", "value.usd": "-266.75"}, {"symbol": "au:GOR", "account": "ISSAUSWAP", "shares": "-283", "price.usd": "1.24", "value.usd": "-350.92"}, {"symbol": "au:PME", "account": "ISSAUSWAP", "shares": "-29", "price.usd": "18.2", "value.usd": "-527.8"}, {"symbol": "au:IMD", "account": "ISSAUSWAP", "shares": "602", "price.usd": "0.94", "value.usd": "565.88"}, {"symbol": "au:COE", "account": "ISSAUSWAP", "shares": "2197", "price.usd": "0.27", "value.usd": "593.19"}, {"symbol": "au:API", "account": "ISSAUSWAP", "shares": "-850", "price.usd": "0.81", "value.usd": "-688.5"}]}, {"type": "text", "data": "ISSAUSWAP NetPos: 290K, AbsPos: 4.13M\\nTOTAL NetPos: 290K AbsPos: 4.13M"}]""")
    print("Done", file=sys.stderr)



@cli.command(name="plot_pnl")
@click.option("--date")
@click.option("--symbols", multiple=True)
@click.option("--nodes", multiple=True, type=int)
def plot_pnl(date=None, symbols=None, nodes=[]):
    print("Getting data", file=sys.stderr)
    time.sleep(1)
    print("""[{"type": "plot", "title": "RING PnL", "xlabel": "Time", "xtype": "ms", "ylabel": "Total PnL", "date": "2020-09-02", "plots": [{"X": ["34236241", "34258350", "34298230", "34338489", "34382844", "34403934", "34422798", "34442403"], "Y": ["144.69", "144.69", "144.69", "179.14", "179.14", "179.14", "213.59", "213.59"]}]}]""")
    print("Done", file=sys.stderr)

if __name__ == '__main__':
    cli()
