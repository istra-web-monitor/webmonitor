# > require(jsonlite); dd <- get.istra.ports(sched.names=regions.to.sched.names("America"), with.params=TRUE); colnames(dd) <- gsub("\\.", "_", colnames(dd)); list(America=dd) %>% jsonlite::toJSON() %>% writeLines(con=paste0("~/Desktop/ports.json"))
import json, os

def get_ports(region):
    filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "ports.json")
    if not os.path.exists(filename):
        filename = "/cfs2/ilan/ports.json"  # DEBUGDIR
    with open(filename, "rt") as f:
        data = json.load(f)

    for row in data.get(region, []):
        yield row

