import os, csv

# dd <- with.options(monitor.log.server="shob", get.monitor.latest.per.strat(date=20200228)) %$% data.frame.no.factors(symbol=symbol, node=node, strategyTag=strategyTag, bSuspended=bSuspended, region=assets.to.regions(get.symbol.asset(symbol), unique=FALSE)); dd <- dd[!duplicated(dd[,c("symbol","node")], fromLast=T),]; write.table(dd, "/cfs2/USERNAME/symbols.csv", row.names=FALSE, sep=",", quote=FALSE); write.table(dd, "~/repo.web/web_intervention/symbols.csv", row.names=FALSE, sep=",", quote=FALSE)
def get_symbols(region):
    filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "symbols.csv")
    if not os.path.exists(filename):
        filename = "/cfs2/ilan/symbols.csv"  # DEBUGDIR
    with open(filename, "rt") as f:
        for row in csv.DictReader(f):
            if row["region"] != region:
                continue
            row["bSuspended"] = row["bSuspended"] == "TRUE"
            del row["region"]
            yield row


def get_tre_symbols_and_groups(region):
    filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "tre.limits.symbols.summary.csv")
    if not os.path.exists(filename):
        filename = "/cfs2/ilan/tre.limits.symbols.summary.csv"  # DEBUGDIR
    with open(filename, "rt") as f:
        for row in csv.DictReader(f):
            yield {k.replace(".", "_"): row[k] for k in ["symbol", "trader.family", "strategy", "group.name", "node.id"]}

