#!/usr/istra/bin/pypy3

import json, os, csv, datetime, pytz
from typing import List

class DictReaderStrip(csv.DictReader):
    @property                                    
    def fieldnames(self):
        if self._fieldnames is None:
            csv.DictReader.fieldnames.fget(self)
            if self._fieldnames is not None:
                self._fieldnames = [name.strip() for name in self._fieldnames]
        return self._fieldnames
    
    def __next__(self):
        d = super().__next__()
        return {k: v.strip() for k, v in d.items()}

def get_lmm_symbols():
    from configs.paths import current_repo_path
    import math
    path = os.path.join(current_repo_path, "R", "istrainfra", "data", "lmm.symbols.csv")
    symbols = set()

    def parse_date(d):
        if d == "-Inf":
            return -math.inf
        if d == "Inf":
            return math.inf
        return int(d)

    with open(path, "rt") as f:
        reader = DictReaderStrip(f)
        for row in reader:
            today = int(datetime.datetime.today().strftime("%Y%m%d"))
            if parse_date(row["from"]) <= today < parse_date(row["to"]):
                symbols.add(row["symbol"])
    return list(symbols)
    
def save_regions():
    from configs import regions
    sched_names = lambda r: [s.name for s in r.sched_names]
    rle_accounts = lambda r: ["@GLOBAL"] + (["Pundion", "IssarCA", "USFutures"] if r.name == "America" else sched_names(r))
    lmm_symbols = lambda r: get_lmm_symbols() if r.name == "America" else list()
    data = [
            dict(
                timezone=str(r.tzinfo),
                name=r.name,
                sched_names=sched_names(r),
                rle_servers=[rs.hostname for rs in r.rle_servers],
                rle_accounts=rle_accounts(r),
                lmm_symbols=lmm_symbols(r),
                servers=[dict(hostname=s.hostname,
                              sched_name=s.sched_name.name,
                              server_type=str(s.server_type),
                              nodes=[dict(id=n.node_id, role=str(n.role), pretty_name=n.pretty_name, is_special=n.is_special, **{k: v for k, v in n.__dict__.items() if k.startswith("is_")}) for n in s.nodes.values()])
                              for s in r.servers if str(s.server_type) in ('PRODUCTION', 'INTEGRATION', 'RECORDING')]
                ) for r in regions.REGIONS.values()]

    with open("/cfs2/ilan/regions.json", "wt") as f:  # DEBUGDIR
        json.dump(data, f)

def get_regions():
    filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "regions.json")
    if not os.path.exists(filename):
        filename = "/cfs2/ilan/regions.json"  # DEBUGDIR
    with open(filename, "rt") as f:
        return json.load(f)

def get_timezone(region: str):
    return pytz.timezone('US/Eastern')

def get_current_region() -> str:
    return os.environ.get("REGION", "America")

def get_production_trading_nodes(r: str) -> List[int]:
    return [11701]

if __name__ == '__main__':
    save_regions()
