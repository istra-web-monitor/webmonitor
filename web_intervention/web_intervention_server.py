#!/usr/istra/bin/pypy3
import os, sys, subprocess, json, datetime, pipes
from collections import defaultdict
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi import Depends, FastAPI, HTTPException, APIRouter, Header
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.gzip import GZipMiddleware
from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_403_FORBIDDEN
from starlette.templating import Jinja2Templates
from starlette.requests import Request
from starlette.responses import HTMLResponse, Response
from starlette.types import Scope
from typing import List, Iterable, Tuple, TYPE_CHECKING, Optional as tOptional, Union, Any, TextIO, Dict
from typing_extensions import TypedDict
from starlette.staticfiles import StaticFiles

import celery as celery_lib
import redis, socket, hashlib
from celery.utils import uuid
from celery.utils.log import get_task_logger
from celery.exceptions import Ignore
from enum import Enum
try:
    from ProcessUtils import set_no_blocking
except:
    def set_no_blocking(p): #type: ignore[misc]
        pass

logger = get_task_logger(__name__)

# Initialize Redis Celery
for redis_host in ("localhost", "redissrv2"):
    try:
        redis.Redis(redis_host, socket_connect_timeout=1).ping()
        break
    except Exception:
        pass
else:
    raise Exception("Couldn't find available Redis server")

logger.info(f"Using Redis server {redis_host}")
BROKER_TRANSPORT_OPTIONS = {'socket_timeout': 10}
redis_host_number = 1 if redis_host == "localhost" else int(hashlib.sha256(socket.gethostname().encode('utf-8')).hexdigest(), 16) % 1000
celery = celery_lib.Celery("web_intervention_server", broker=f'redis://{redis_host}:6379/{redis_host_number}', backend=f'redis://{redis_host}:6379/{redis_host_number}',
                           redis_socket_timeout=30, redis_socket_connect_timeout=30, redis_retry_on_timeout=True)

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(LOCAL_DIR))

CLIENT_FOLDER = os.environ.get("WIS_CLIENT_DEPLOY_FOLDER", "/mnt/shared/web_intervention_client")

_MOCK = os.environ.get("MOCK_INTERVENTOR", "no") == "yes"
if not _MOCK:
    try:
        from web_intervention.reals import regions
    except Exception as ex:
        logger.warning("Can't import reals folder - moving to mock. {ex}")
        _MOCK = True

if _MOCK:
    if not TYPE_CHECKING:
        from web_intervention.mocks import mock_regions as regions
        from web_intervention.mocks import mock_users as users
        from web_intervention.mocks import mock_symbols as symbols
        from web_intervention.mocks import mock_ports as ports
        from web_intervention.mocks.mock_exchanges import EXCHANGES
        from web_intervention.mocks.mock_feeds import FEEDS
        from web_intervention.reals import current_date  # reals is OK here.
else:
    from web_intervention.reals import regions
    from web_intervention.reals import users
    from web_intervention.reals import symbols
    from web_intervention.reals.exchanges import EXCHANGES
    from web_intervention.reals.feeds import FEEDS
    from web_intervention.reals import ports
    from web_intervention.reals import current_date
    from python import TimeUtils

_USE_INTERVENTOR = not _MOCK and os.environ.get("USE_INTERVENTOR", "yes") != "no" 
_REAL_INTERVENTOR = not _MOCK and os.environ.get("REAL_INTERVENTOR", "yes" if redis_host == "localhost" else "no") != "no"

from web_intervention.history import History, HistoryItem
from pydantic import BaseModel

if _USE_INTERVENTOR:
    from web_intervention import interventor
    from ShellUtils.Shellathon.Scmd import Scmd
    from ShellUtils.Shellathon import shellathon
    logger.info(f"Using interventor. Real mode={_REAL_INTERVENTOR}")

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_middleware(
    GZipMiddleware,
    minimum_size=1000
)

security = HTTPBasic()
templates = Jinja2Templates(directory=CLIENT_FOLDER)
router = APIRouter()
history = History()

class InterventionTask(celery_lib.Task): #pylint: disable=abstract-method
    pass  # TODO - add on_failure handling

class LogDataTypedDict(TypedDict):
    server: tOptional[str]
    return_code: tOptional[int]
    status: tOptional[str]
    stdout: str
    stderr: str

def LogData() -> LogDataTypedDict:
    return dict(server=None, return_code=None, status=None, stdout="", stderr="")

def update_log_data(current: LogDataTypedDict, other: LogDataTypedDict) -> Union[tOptional[str], bool]:
    rv = other['stdout'] or other['stderr'] or other['return_code'] != current['return_code'] or other['status'] != current['status']

    current["server"] = other['server']
    current["return_code"]= other['return_code']
    current["status"] = other['status']
    current["stdout"] += other['stdout']
    current["stderr"] += other['stderr']
    return rv

def _split(prefix: str, val) -> List[str]:
    if isinstance(val, str):
        val = val.split(",")
    cmd = []
    for v in val:
        cmd += [prefix, str(v)]
    return cmd

def _get_lines(f: TextIO) -> Iterable[str]:
    try:
        while True:
            line = f.readline()
            if not line:
                return
            yield line
    except IOError:
        pass
        
def list2cmd(args: Iterable[str]) -> str:
    return ' '.join(pipes.quote(arg) for arg in args)

def is_intervention_is_get_command(intervention_name: str) -> bool:
    return intervention_name.startswith('get') or intervention_name.startswith('plot')

def build_interventor_command_mock(intervention_name: str, params: Dict[str, Any], region: str) -> str:
    cmd = [os.path.join(LOCAL_DIR, "mocks", "mock_interventor.py"), intervention_name]
    for p in ('nodes', 'servers', 'symbols', 'strategies', 'strats', 'mh_servers', 'exchanges', 'ports'):
        cmd += _split(f"--{p}", params.get(p) or [])
    for p in ('action', 'mode', 'date', 'region'):
        if p in params:
            cmd.append(f'--{p}')
            cmd.append(params[p])
    for p in ('restart_nodes', ):
        if params.get(p, False):
            cmd.append(f'--{p}')
    
    if intervention_name in ("liquidate", "exit_gracefully"):
        cmd += ["--node_symbols", params["node_symbols"].json()]

    return list2cmd(cmd)

def add_intervention_mock(intervention_log:str, region: str, user: str):
    timestr = datetime.datetime.now(tz=regions.get_timezone(region)).strftime("%H:%M:%S")
    intervention_text = f"{timestr} - {user} - {intervention_log.strip()}"
    logger.info(intervention_text)

def build_interventor_command_real(intervention_name: str, params: Dict[str,Any], region: str) -> str:
    params.pop("parallel", None)
    for js_name, py_name in (('restart', 'force_restart'), ('variant', 'variant_name')):
        if js_name in params:
            params[py_name] = params[js_name]
            del params[js_name]
    
    if "variant_name" in params:
        if params["variant_name"] == "":
            params["variant_name"] = None
        else:
            params["variant_type"] = "specific"

    postpend = ""
    if is_intervention_is_get_command(intervention_name):
        postpend += "--outfile"
    else:
        postpend += "--json"

    if _REAL_INTERVENTOR:
        postpend += " --real"

    if intervention_name == "tre_unsuspend_and_restart":
        groups_set = set(params["groups"])
        data = [row for row in symbols.get_tre_symbols_and_groups(region=region) if row["group_name"] in groups_set or len(groups_set) == 0]
        if len(data) == 0:
            raise Exception(f"TRE Unsuspend and restart: couldn't find any valid group in sched. Groups: {', '.join(params['groups'])}")
        nodes = list({int(row['node_id']) for row in data})
        per_strat_symbols_dict = defaultdict(set)
        for row in data:
            per_strat_symbols_dict[row['strategy']].add(row['symbol'])  # TODO - alias to concrete
        per_strat_symbols_list = [(k, list(v)) for k, v in per_strat_symbols_dict.items()]
        date = datetime.datetime.now(tz=regions.get_timezone(region)).date()
        params = dict(nodes=nodes, per_strat_symbols=per_strat_symbols_list, date=date)
        intervention_name = "exclusion_symbols_json"

    interventor_func = getattr(interventor, intervention_name)
    cmdline = shellathon.get_cmdline(interventor_func)
    if cmdline is None:
        raise NotImplementedError(f"Unknown intervention name {intervention_name}")

    return Scmd(
        func=interventor_func,
        func_kws=params,
        postpend=postpend,
    )
    
def add_intervention_real(intervention_log, region, user):
    timestr = TimeUtils.now(tz=regions.get_timezone(region)).strftime("%H:%M:%S")
    intervention_text = f"{timestr} - {user} - {intervention_log.strip()}"
    servers = regions.get_production_servers(region)
    postpend = "--real" if _REAL_INTERVENTOR else ""

    cmd = Scmd(interventor.add_intervention,
               func_kws=dict(servers=servers, intervention_text=intervention_text),
               postpend=postpend,
    )
    output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    logger.info(f"Ran intervention. Output:\n{output[0]}\n{output[1]}\n")

build_interventor_command = build_interventor_command_real if _USE_INTERVENTOR else build_interventor_command_mock
add_intervention = add_intervention_real if _USE_INTERVENTOR else add_intervention_mock

@celery.task(bind=True, base=InterventionTask)
def do_intervention(self, intervention_name, params, region, user, intervention_log=None):
    logger.info("Starting intervention. user: {}, name: {}, params: {}".format(user, intervention_name, params))

    cmd = build_interventor_command(intervention_name, params, region=region)
    data = defaultdict(LogData)

    if intervention_log:
        logger.info(f"Adding intervention {intervention_log}")
        add_intervention(intervention_log, region=region, user=user)
    logger.info(f"Running {cmd}")

    def add_global_log(line):
        log_data = dict(server="GLOBAL", return_code=None, status=None, stdout="", stderr=line)
        if update_log_data(data[log_data["server"]], log_data):
            self.update_state(state='PROGRESS', meta=dict(data=data))

    def add_data_log(line):
        log_data = dict(server="data", return_code=None, status=None, stdout=line, stderr="")
        if update_log_data(data[log_data["server"]], log_data):
            self.update_state(state='PROGRESS', meta=dict(data=data))
    
    add_global_log(f"Running:\n{cmd}\n")
    is_get_command = is_intervention_is_get_command(intervention_name)
    with subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8", universal_newlines=True, bufsize=1) as p:

        set_no_blocking(p.stdout)
        set_no_blocking(p.stderr)
        def read_output():
            for line in _get_lines(p.stdout):
                if is_get_command:
                    add_data_log(line)
                else:
                    try:
                        log_data = json.loads(line)
                        if update_log_data(data[log_data["server"]], log_data):
                            self.update_state(state='PROGRESS', meta=dict(data=data))
                    except json.JSONDecodeError:
                        add_global_log(f"ERROR - Could not parse JSON line - {line}")

            for line in _get_lines(p.stderr):
                logger.info("Got stderr: {}".format(line.strip()))
                add_global_log(line)

        while True:
            try:
                p.wait(1)
            except subprocess.TimeoutExpired:
                pass
            read_output()
            if p.poll() is not None:
                read_output()
                break

    logger.info("Finished intervention. name: {}, params: {}".format(intervention_name, params))
    if p.returncode == 0:
        return data
    else:
        self.update_state(state='FAILURE', meta=dict(
            exc_type="Exception",
            exc_message="Failed to run",
            custom=data))
        raise Ignore()

def get_current_username(credentials: HTTPBasicCredentials = Depends(security)):
    if not users.verify_password(username=credentials.username, password=credentials.password):
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username

@router.get("/users/me")
def read_current_user(username: str = Depends(get_current_username)):
    is_read_only = (username in users.READ_ONLY_USERS)
    return {"username": username, "read_only": is_read_only}

@router.get("/users/read_only_users")
def read_only_users():
    return {"read_only_users": users.READ_ONLY_USERS}

@app.get("/regions/current_region")
def read_current_region(request: Request):
    region = regions.get_current_region()
    return {"region": region}

class VariantTypeEnum(str, Enum):
    auto = 'auto'
    specific = 'specific'
    reset = 'reset'
    next = 'next'

class RunningModeEnum(str, Enum):
    sim = 'sim'
    prod = 'prod'

class StartNodesData(BaseModel):
    nodes: List[int]
    bypass_warnings: bool
    variant: tOptional[str] = None
    restart: bool = False
    running_mode: str = 'prod'
    interactive: bool = False

class JustNodesData(BaseModel):
    nodes: List[int]

class JustServersData(BaseModel):
    servers: List[int]

class SymbolSuspensionStateEnum(str, Enum):
    ALL='ALL'
    ONLY_SUSPENDED='ONLY_SUSPENDED'
    ONLY_NOT_SUSPENDED='ONLY_NOT_SUSPENDED'

class NodeSymbolsData(BaseModel):
    node_symbols: List[Tuple[int, List[str]]]
    symbols_suspension_state: SymbolSuspensionStateEnum = SymbolSuspensionStateEnum.ONLY_SUSPENDED

class ExitModeEnum(str, Enum):
    exit='exit'
    resume='resume'

class NodeSymbolsExitMode(BaseModel):
    node_symbols: List[Tuple[int, List[str]]]
    action: ExitModeEnum = ExitModeEnum.exit
    persistent: bool = True


class OrderEntryStateEnum(str, Enum):
    PORT_DISABLED='PORT_DISABLED'
    PORT_ENABLED='PORT_ENABLED'
    PORT_PASSIVE='PORT_PASSIVE'

class ExclusionActionEnum(str, Enum):
    include='include'
    exclude='exclude'
    reset='reset'
    set='set'
    status='status'
    
class OrderEntryData(BaseModel):
    servers: List[int]
    ports: List[int]
    state: OrderEntryStateEnum
    restart_nodes: bool = False
    action: ExclusionActionEnum

class ActionEnum(str, Enum):
    include='include'
    exclude='exclude'

class ExclusionSymbolsData(BaseModel):
    nodes: List[int]
    action: ActionEnum
    symbols: List[str]
    strats: List[str]
    reason: str
    restart_nodes: bool = False

class FeedExclusionData(BaseModel):
    action: ExclusionActionEnum
    nodes: List[int]
    feeds: List[str]
    restart_nodes: bool = False

class OrderEntryExchangeExclusionData(BaseModel):
    action: ExclusionActionEnum
    mh_servers: List[int]
    exchanges: List[str]
    restart_nodes: bool = False

class OrderEntryLinkExclusionData(BaseModel):
    action: ExclusionActionEnum
    servers: List[int]
    mh_servers: List[int]
    restart_nodes: bool = False

class OmserverModifyOrderActionEnum(str, Enum):
    suppress="suppress"
    unsuppress="unsuppress"
    purge="purge"

class ModifyOrderStateInOMServerData(BaseModel):
    nodes: List[int]
    exchanges: List[str]
    action: OmserverModifyOrderActionEnum
    only_limbo: bool


class LocatesBrokerEnum(str, Enum):
    BAML="BAML"
    CIBC="CIBC"

class LocatesFlowServerData(BaseModel):
    iteration : int
    sched_name : str
    broker : LocatesBrokerEnum
    date: datetime.date
    run_exclude_flow : bool
    send_locates_email : bool

@router.post('/intervention/locates_flow')
async def locates_flow(data: LocatesFlowServerData, region:str = Header(None), username: str = Depends(get_current_username)):
    params = dict(data)
    params['date'] = data.date.strftime("%Y%m%d")
    intervention_log = f"Locates Flow for sched name:{ data.sched_name } Broker:{data.broker} "
    return _run_intervention(intervention_type="locates_flow", params=params, intervention_log=intervention_log, username=username, region=region)

@router.post('/intervention/start_nodes')
async def start_nodes(data: StartNodesData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = (f"""Start nodes. Nodes: {", ".join(str(n) for n in data.nodes)} - """ +
                        (f"explicit variant: {data.variant} " if data.variant else "default variant; ") +
                        ("" if data.restart else "not ") + "forcing restart.")
    return _run_intervention(intervention_type="start_nodes", params=dict(data), intervention_log=intervention_log, username=username, region=region)

@router.post('/intervention/kill_nodes')
async def kill_nodes(data: JustNodesData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Kill nodes. Nodes: {", ".join(str(n) for n in data.nodes)}"""
    return _run_intervention(intervention_type="kill_nodes", params=dict(data), intervention_log=intervention_log, username=username, region=region)

@router.post('/intervention/start_peripherals')
async def start_peripherals(data: JustServersData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Start peripherals. Servers: {", ".join(str(s) for s in data.servers)}"""
    return _run_intervention(intervention_type="start_peripherals", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/start_services')
async def start_services(data: JustServersData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Start services. Servers: {", ".join(str(s) for s in data.servers)}"""
    return _run_intervention(intervention_type="start_services", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/kill_services')
async def kill_services(data: JustServersData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Kill services. Servers: {", ".join(str(s) for s in data.servers)}"""
    return _run_intervention(intervention_type="kill_services", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/kill_peripherals')
async def kill_peripherals(data: JustServersData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Kill peripherals. Servers: {", ".join(str(s) for s in data.servers)}"""
    return _run_intervention(intervention_type="kill_peripherals", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/liquidate')
async def liquidate(data: NodeSymbolsData, region:str = Header(None), username: str = Depends(get_current_username)):
    all_symbols = {symbol for ns in data.node_symbols for symbol in ns[1]}
    intervention_log = f"""Liquidating. Nodes: {", ".join(str(ns[0]) for ns in data.node_symbols)}, Symbols: {", ".join(all_symbols)}"""
    return _run_intervention(intervention_type="liquidate", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/exit_gracefully')
async def exit_gracefully(data: NodeSymbolsExitMode, region:str = Header(None), username: str = Depends(get_current_username)):
    all_symbols = {symbol for ns in data.node_symbols for symbol in ns[1]}
    intervention_log = f"""Exit Gracefully """
    if data.action == "resume":
        intervention_log += f"""- Resume trading - """
    intervention_log += f"""Nodes: {", ".join(str(ns[0]) for ns in data.node_symbols)}, Symbols: {", ".join(all_symbols)}"""
    if data.action == "resume":
        intervention_log += f""" Do not forget to restart cores after command ran - in order to effectively resume live trading"""
    return _run_intervention(intervention_type="exit_gracefully", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/orderentry_port_set')
async def orderentry_port_set(data: OrderEntryData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Set ports. State: {data.state}. Ports: {", ".join(str(p) for p in data.ports)}."""
    if data.servers:
        intervention_log += f" Target servers: {', '.join(str(s) for s in data.servers)}."
    if data.restart_nodes:
        intervention_log += " Restarting nodes."

    return _run_intervention(intervention_type="exclusion_orderentry_port", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/exclusion_feed')
async def exclusion_feed(data: FeedExclusionData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Feed {data.action.title()}. Feeds: {", ".join(data.feeds)}. Nodes: {", ".join(str(n) for n in data.nodes)}."""
    if data.restart_nodes:
        intervention_log += " Restarting nodes."

    return _run_intervention(intervention_type="exclusion_feed", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/exclusion_orderentry_exchange')
async def exclusion_orderentry_exchange(data: OrderEntryExchangeExclusionData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Order Entry Exchange {data.action.title()}. Exchanges: {", ".join(data.exchanges)}. MH Servers: {", ".join(str(s) for s in data.mh_servers)}."""
    if data.restart_nodes:
        intervention_log += " Restarting nodes."

    return _run_intervention(intervention_type="exclusion_orderentry_exchange", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/exclusion_orderentry_link')
async def exclusion_orderentry_link(data: OrderEntryLinkExclusionData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Order Entry Link {data.action.title()}. Servers: {", ".join(str(s) for s in data.servers)}. MH Servers: {", ".join(str(s) for s in data.mh_servers)}."""
    if data.restart_nodes:
        intervention_log += " Restarting nodes."

    return _run_intervention(intervention_type="exclusion_orderentry_link", params=dict(data), intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/modify_order_state_in_omserver')
async def modify_order_state_in_omserver(data: ModifyOrderStateInOMServerData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Modify order state in omserver. Action: {data.action}. {"(Limbo only). " if data.only_limbo else ""}Nodes: {", ".join(str(n) for n in data.nodes)}. exchanges: {", ".join(data.exchanges)}"""

    return _run_intervention(intervention_type="modify_order_state_in_omserver_by", params=dict(data), intervention_log=intervention_log, region=region, username=username)

class RLEActionEnum(str, Enum):
    enable="enable"
    disable="disable"
    unblock="unblock"
    increase_pnl_limit="increase_pnl_limit"
    restore_pnl_limit="restore_pnl_limit"
    increase_position_limit="increase_position_limit"
    restore_position_limit="restore_position_limit"

class SendViaEnum(str, Enum):
    ssh="ssh"
    chat_server="chat_server"

class SetRLEData(BaseModel):
    servers: List[str]
    action: RLEActionEnum
    account: str = "@GLOBAL"
    send_via: SendViaEnum

@router.post('/intervention/set_rle_server')
async def set_rle_server(data: SetRLEData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Set RLE Server. Action: {data.action}. Account: {data.account}. Send via: {data.send_via}. Servers: {", ".join(str(s) for s in data.servers)}."""

    return _run_intervention(intervention_type="set_rle_server", params=dict(data), intervention_log=intervention_log, region=region, username=username)

class Baml200BytesEnum(str, Enum):
    strat='start'
    stop='stop'
    restart='restart'
    restart_and_resend_files='restart_and_resend_files'
    create_file='create_file'
    create_and_send_to_ftp='create_and_send_to_ftp'

class SetBaml200Bytes(BaseModel):
    shob_server: str
    action: Baml200BytesEnum
    date: tOptional[datetime.date] = None

@router.post('/intervention/baml_200_bytes')
async def set_baml_200bytes(data: SetBaml200Bytes, region:str = Header(None), username: str = Depends(get_current_username)):
    if region != "America":
        raise Exception("Region must be America")

    intervention_log = f"""Set BAML 200bytes. Action: {data.action}. Server: {data.shob_server}"""
    params = dict(data)
    if params['date'] is None:
        params['date'] = current_date.get_date(regions.get_timezone(region))
    params['date'] = params['date'].strftime("%Y%m%d")  # type: ignore[union-attr]
    return _run_intervention(intervention_type="set_baml_200bytes", params=params, intervention_log=intervention_log, region=region, username=username)


class GetBaml200BytesStatus(BaseModel):
    date: tOptional[datetime.date] = None
    shob_server: str

@router.post('/intervention/get_baml_200bytes_status')
async def get_baml_200bytes_status(data: GetBaml200BytesStatus, region:str = Header(None), username: str = Depends(get_current_username)):
    if region != "America":
        raise Exception("Region must be America")

    params = dict(data)
    if params['date'] is None:
        params['date'] = current_date.get_date(regions.get_timezone(region))
    params['date'] = params['date'].strftime("%Y%m%d")  # type: ignore[union-attr]
    return _run_intervention(intervention_type="get_baml_200bytes_status", params=params, region=region, username=username)


class RLEDaemonActionEnum(str, Enum):
    status="status"
    unblock="unblock"
    heartbeat="heartbeat"

class SetRLEDaemonData(BaseModel):
    servers: List[int]
    action: RLEDaemonActionEnum

@router.post('/intervention/set_rle_daemon')
async def set_rle_daemon(data: SetRLEDaemonData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Set RLE Daemon. Action: {data.action}. Servers: {", ".join(str(s) for s in data.servers)}."""

    return _run_intervention(intervention_type="set_rle_daemon", params=dict(data), intervention_log=intervention_log, region=region, username=username)


class ResetOMServerData(BaseModel):
    servers: List[int]

@router.post('/intervention/restart_and_reset_omserver')
async def restart_and_reset_omserver(data: ResetOMServerData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Restart and reset omserver on servers: {", ".join(str(s) for s in data.servers)}."""

    return _run_intervention(intervention_type="restart_and_reset_omserver", params=dict(data), intervention_log=intervention_log, region=region, username=username)


class ACRSStateEnum(str, Enum):
    Enabled="Enabled"
    RejectAllOrders="RejectAllOrders"
    AllowAllOrders="AllowAllOrders"

class SetACRSData(BaseModel):
    servers: List[int]
    state: ACRSStateEnum

@router.post('/intervention/set_acrs')
async def set_acrs(data: SetACRSData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Set ACRS. State: {data.state}. Servers: {", ".join(str(s) for s in data.servers)}."""

    return _run_intervention(intervention_type="set_acrs", params=dict(data), intervention_log=intervention_log, region=region, username=username)

class PreventOvernightPositionData(BaseModel):
    nodes: List[int]
    symbols: List[str]
    tre_group_name: str
    persistent: bool

@router.post("/intervention/prevent_overnight_position")
async def prevent_overnight_position(data:PreventOvernightPositionData,
                                     region:str = Header(None),
                                     username:str = Depends(get_current_username)):
    intervention_log = (f"Preventing overnight position for {data.symbols} on {data.nodes} "
                        f"TRE group:{data.tre_group_name}, persistent:{data.persistent}")
    return _run_intervention(intervention_type="prevent_overnight_position",
                             params=dict(data),
                             intervention_log=intervention_log,
                             region=region,
                             username=username)

class TREGroupModeEnum(str, Enum):
    GROUPS="GROUPS"
    ALL_GROUPS="ALL_GROUPS"

class TREStateEnum(str, Enum):
    enable="enable"
    disable="disable"

class SetTREGroupData(BaseModel):
    state: TREStateEnum
    mode: TREGroupModeEnum
    nodes: List[int]
    groups: List[str]

@router.post('/intervention/set_tre')
async def set_tre(data: SetTREGroupData, region:str = Header(None), username: str = Depends(get_current_username)):
    if len(data.nodes) == 0:
        data.nodes = regions.get_production_trading_nodes(region)
    intervention_log = f"""Set TRE. State: {data.state}. Nodes: {", ".join(str(n) for n in data.nodes)}."""
    if data.mode == TREGroupModeEnum.ALL_GROUPS:
        intervention_log += " All groups."
    else:
        intervention_log += f""" Groups: {", ".join(data.groups)}"""

    return _run_intervention(intervention_type="set_tre", params=dict(data), intervention_log=intervention_log, region=region, username=username)

class TREUnsuspendData(BaseModel):
    groups: List[str]

@router.post('/intervention/tre_unsuspend_and_restart')
async def tre_unsuspend_and_restart(data: TREUnsuspendData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""TRE unsuspend and restart. Groups: {", ".join(data.groups)}."""
    return _run_intervention(intervention_type="tre_unsuspend_and_restart", params=dict(data), intervention_log=intervention_log, region=region, username=username)


class UpdateIniData(BaseModel):
    key: str
    value: str
    comment: str
    nodes: List[int]

@router.post('/intervention/update_ini_config')
async def update_ini_config(data: UpdateIniData, region:str = Header(None), username: str = Depends(get_current_username)):
    cnst = f"{data.key.strip()} = {data.value.strip()}"
    intervention_log = f"""Updating ini config: {cnst}. Nodes: {", ".join(str(n) for n in data.nodes)}."""

    params = dict(data)
    params["cnst"] = cnst
    timestr = datetime.datetime.now(tz=regions.get_timezone(region)).strftime("%H:%M:%S")
    params["comment"] = f"""{params["comment"]} {username} {timestr}"""
    del params["key"]
    del params["value"]
    return _run_intervention(intervention_type="update_ini_config", params=params, intervention_log=intervention_log, region=region, username=username)

class AutoShortMarkingActionEnum(str, Enum):
    allow_some="allow_some"
    disallow_some="disallow_some"
    set_disallowed="set_disallowed"

class AutoShortMarkingReconfigureData(BaseModel):
    action: AutoShortMarkingActionEnum 
    nodes: List[int]
    symbols: List[str]

@router.post('/intervention/auto_short_marking_reconfigure')
async def auto_short_marking_reconfigure(data: AutoShortMarkingReconfigureData, region:str = Header(None), username: str = Depends(get_current_username)):
    intervention_log = f"""Auto short marking reconfigure. Action: {data.action}, nodes: {", ".join(str(n) for n in data.nodes)}. Symbols: {", ".join(data.symbols)}"""

    return _run_intervention(intervention_type="auto_short_marking_reconfigure", params=dict(data), intervention_log=intervention_log, region=region, username=username)


@router.post('/intervention/exclusion_symbols')
async def exclusion_symbols(data: ExclusionSymbolsData, region:str = Header(None), username: str = Depends(get_current_username)):
    if data.nodes == [0]:
        symbols_set = set(data.symbols)
        strats_set = set(data.strats)
        data.nodes = list({row['node'] for row in symbols.get_symbols(region=region) if row["symbol"] in symbols_set and row["strategyTag"] in strats_set})

    params = dict(data)
    intervention_log = f"""{data.action.title()} strategy-symbols. Reason: {data.reason}. Nodes: {", ".join(str(n) for n in data.nodes)}, Symbols: {", ".join(data.symbols)}. Strategies: {", ".join(data.strats)}"""
    if data.restart_nodes:
        intervention_log += " Restarting nodes."
        params["nodes_to_restart"] = params["nodes"]
    return _run_intervention(intervention_type="exclusion_symbols", params=params, intervention_log=intervention_log, region=region, username=username)

@router.post('/intervention/symbols_exclusion_status')
async def symbols_exclusion_status(data: JustServersData, region:str = Header(None), username: str = Depends(get_current_username)):
    return _run_intervention(intervention_type="get_symbols_exclusion_status", params=dict(data), region=region, username=username)

class JpCreateManualLiquidateionFile(BaseModel):
    date: datetime.date
    price_in_usd: bool
    price_aggressiveness: str

@router.post('/intervention/get_jp_manual_liquidateion_file')
async def get_jp_manual_liquidateion_file(data: JpCreateManualLiquidateionFile, region:str = Header(None), username: str = Depends(get_current_username)):
    params = dict(data)
    params['date'] = data.date.strftime("%Y%m%d")

    return _run_intervention(intervention_type="get_jp_manual_liquidateion_file", params=params, region=region, username=username)

class OmserverModeEnum(str, Enum):
    brief="brief"
    positions="positions"
    orders="orders"
    inflight="inflight"
    rejected="rejected"
    slow="slow"
    limbo="limbo"
    suppressed="suppressed"

class OmserverSummaryData(BaseModel):
    servers: List[int]
    mode: OmserverModeEnum

@router.post('/intervention/get_omserver_summary')
async def get_omserver_summary(data: OmserverSummaryData, region:str = Header(None), username: str = Depends(get_current_username)):
    return _run_intervention(intervention_type="get_omserver_summary", params=dict(data), region=region, username=username)

@router.post('/intervention/get_position')
async def get_position(region:str = Header(None), username: str = Depends(get_current_username)):
    return _run_intervention(intervention_type="get_position", params=dict(region=region), region=region, username=username)

class PlotLMMData(BaseModel):
    symbols: List[str]
    date: datetime.date

@router.post('/intervention/plot_lmm_levels')
async def plot_lmm_levels(data: PlotLMMData, region:str = Header(None), username: str = Depends(get_current_username)):
    params = dict(data)
    params['date'] = data.date.strftime("%Y%m%d")
    return _run_intervention(intervention_type="plot_lmm_levels", params=params, region=region, username=username)

class PlotPnlData(BaseModel):
    symbols: List[str]
    date: datetime.date
    nodes: List[int]

@router.post('/intervention/plot_pnl')
async def plot_pnl(data: PlotPnlData, region:str = Header(None), username: str = Depends(get_current_username)):
    params = dict(data)
    params['date'] = data.date.strftime("%Y%m%d")
    return _run_intervention(intervention_type="plot_pnl", params=params, region=region, username=username)


class GetExecutionsData(BaseModel):
    date: datetime.date
    servers: List[int] = []
    nodes: List[int] = []
    exchanges: List[str] = []
    ports: List[int] = []
    symbols: List[str] = []

@router.post('/intervention/get_executions')
async def get_executions(data: GetExecutionsData, region:str = Header(None), username: str = Depends(get_current_username)):
    params = dict(data)
    params['date'] = data.date.strftime("%Y%m%d")
    return _run_intervention(intervention_type="get_executions", params=params, region=region, username=username)

class ExclusionStatusData(BaseModel):
    servers: List[int] = []

@router.post('/intervention/exclusion_status')
async def exclusion_status(data: ExclusionStatusData, region:str = Header(None), username: str = Depends(get_current_username)):
    return _run_intervention(intervention_type="exclusion_status", params=dict(data), region=region, username=username)

@router.post('/intervention/{intervention_type}')
async def intervention(intervention_type: str, request: Request) -> dict:
    try:
        data = await request.json()
    except json.JSONDecodeError:
        data = await request.form()
    
    return _run_intervention(intervention_type=intervention_type, params=dict(data))

def _run_intervention(intervention_type,
                      params,
                      region: str = None,
                      intervention_log: tOptional[str] = None,
                      username: tOptional[str] = None):
    if username in users.READ_ONLY_USERS:
        raise HTTPException(
            status_code=HTTP_403_FORBIDDEN,
            detail=f"Error: user {username} is not allowed to send interventions.")
    logger.info(f"Running {intervention_type} with params: {params}")
    task = do_intervention.apply_async(
        task_id=f"{intervention_type}-{uuid()}",
        kwargs=dict(intervention_name=intervention_type,
                    params=params,
                    intervention_log=intervention_log,
                    user=username,
                    region=region)
    )
    if celery.conf.task_always_eager:
        return task.get()
    return dict(task_id=task.id, status=f'/interventions/{task.id}/status')

def get_intervention_status(task_id: str) -> dict:
    task = do_intervention.AsyncResult(task_id)
    if task.state == "FAILURE":
        meta = task.backend.get(task.backend.get_key_for_task(task.id))
        result_data = json.loads(meta.decode('utf8'))['result']
        result = result_data['custom'] if "custom" in result_data else result_data
    else:
        result = task.result
        if isinstance(result, dict) and 'data' in result and len(result) == 1:
            result = result['data']

    return dict(
        state=task.state,
        id=task.id,
        successful=task.successful(),
        result=result,
    )

@router.get('/interventions/{task_id}/status')
async def intervention_status(task_id: str) -> dict:
    return get_intervention_status(task_id)

@router.post('/regions/{region}/history')
async def add_history(data: HistoryItem) -> HistoryItem:
    history.add(data)
    return data

@router.put('/regions/{region}/history/{id}')
async def update_history(id: str, data: HistoryItem) -> HistoryItem:
    history.update(id=id, data=data)
    return data

@router.get('/regions/{region}/history')
async def get_history(region:str, start_time: datetime.datetime, end_time: datetime.datetime) -> List[HistoryItem]:
    return history.get(region=region, start_time=start_time, end_time=end_time)

@router.get('/regions/{region}/history/{id}')
async def get_history_by_id(region: str, id: str) -> HistoryItem:
    return history.get_by_id(region=region, id=id)

@router.get('/regions/{region}/symbols')
async def get_symbols(region: str) -> dict:
    return dict(symbols=list(symbols.get_symbols(region=region)))

@router.get('/regions/{region}/tre_groups')
async def tre_groups(region: str) -> dict:
    return dict(tre_groups=list(symbols.get_tre_symbols_and_groups(region=region)))

@router.get('/regions/{region}/ports')
async def get_ports(region: str) -> dict:
    return dict(ports=list(ports.get_ports(region=region)))

@router.get('/regions/{region}/exchanges')
async def get_exchanges(region: str) -> dict:
    return dict(exchanges=EXCHANGES[region])

@router.get('/regions/{region}/feeds')
async def get_feeds(region: str) -> dict:
    return dict(feeds=FEEDS[region])

@router.delete('/interventions/{task_id}')
async def cancel_intervention(task_id: str):
    task = do_intervention.AsyncResult(task_id)

    task.revoke(terminate=True)
    return get_intervention_status(task_id)

@router.get('/regions')
async def list_regions():
    return regions.get_regions()

app.include_router(router, dependencies=[Depends(get_current_username)])

## static files ##
class SPAStaticFiles(StaticFiles):
    async def get_response(self, path: str, scope: Scope) -> Response:
        response = await super().get_response(path, scope)
        if response.status_code == 404:
            response = await super().get_response('.', scope)
        return response

async def root(request: Request):
    with open(os.path.join(CLIENT_FOLDER, "index.html"), "rt") as f:
        return HTMLResponse(f.read())
app.mount("/img/", SPAStaticFiles(directory=os.path.join(CLIENT_FOLDER, "assets", "img"), html=True), name="client_img")
app.mount("/help/", SPAStaticFiles(directory=os.path.join(CLIENT_FOLDER, "assets", "help"), html=True), name="client_help")
app.add_route("/", root, methods=["GET"])  # redirect all other traffic to root
app.mount("/", SPAStaticFiles(directory=CLIENT_FOLDER, html=True), name="client")

if __name__ == '__main__':
    import uvicorn
    app.debug = True
    uvicorn.run(app, host="0.0.0.0", port=5000, loop='asyncio', log_level="info")
